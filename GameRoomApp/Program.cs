﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using GameRoomApp.DataLayer.Service;
using GameRoomApp.DataLayer.Service.GameService;
using GameRoomApp.DataLayer.Service.RuleServicePack;
using GameRoomApp.DataLayer.Utils;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GameRoomApp
{
    public class Program
    { 
        public static void Main(string[] args)
        {

            DatabasePopulator databasePopulator = new DatabasePopulator();  
            databasePopulator.Populate();

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
