﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.ImageRepo;
using GameRoomApp.DataLayer.Service.ImageServicePack;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameRoomApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IImageService _imageService;
        private const int NUMBER_OF_OBJID_CHARACTERS = 24;

        public ImageController(IImageService imageService)
        {
            this._imageService = imageService;
        }

        // GET: api/Image
        [HttpGet]
        public IActionResult Get([FromQuery] string OnlyGames, [FromQuery] string Game, [FromQuery] string Last)
        {
            if (Last != null) { return Ok(_imageService.GetLast10()); }
            if (OnlyGames != null)
            {
                return Ok(_imageService.FindGamesName());
            }
            if (Game != null)
            {
                return Ok(_imageService.FindByGame(Game));
            }
            return Ok(_imageService.FindAll());
        }

        // POST: api/Image
        [HttpPost]
        public IActionResult Post([FromBody] ImageMoment imageFromBody)
        {
            ImageMoment image = default(ImageMoment);
            image = _imageService.Insert(imageFromBody);
            if (image != null)
            {
                return Ok("Image Inserted");
            }
            return BadRequest("Error to insert image!");
        }

        // PUT: api/Image/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            ImageMoment image = default(ImageMoment);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId incorect!!");
            }
            image = _imageService.FindById(id);
            if (image != null)
            {
                _imageService.Remove(id);
                return Ok("Image Removed");
            }
            return Ok("Error in Remove Image");
        }
    }
}
