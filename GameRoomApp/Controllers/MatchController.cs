﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Service;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace GameRoomApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private const int NUMBER_OF_OBJID_CHARACTERS = 24;
        private readonly IMatchService _matchService;

        public MatchController(IMatchService matchService)
        {
            this._matchService = matchService;
        }

        // GET: api/Match
        [HttpGet]
        public IActionResult Get([FromQuery] string Id, [FromQuery] string Name, [FromQuery] string Type, [FromQuery] string IdChampionship, [FromQuery] string Last)
        {
            if(Last != null) { return Ok(_matchService.GetLast10()); }
            if (Id != null) { return Ok(_matchService.FindById(Id)); }
            if (Name != null && Type != null) { return Ok(_matchService.FindByNameAndType(Name, Type)); }
            if(IdChampionship != null) { return Ok(_matchService.FindByChampionship(IdChampionship)); }
            return Ok(_matchService.FindAll());
        }

        // POST: api/Match
        [HttpPost]
        public string Post([FromBody] Match matchFromBody)
        {
            Match match = default(Match);
            match = _matchService.Insert(matchFromBody);
            if (match != null)
            {
                return "Match added!!";
            }
            return "Error to add Match!!";
        }

        // PUT: api/Match/5
        [HttpPut("{id}")]
        public string Put(string id, [FromBody] Match matchFromBody)
        {
            Match match = default(Match);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return "ObjectId Incorect";
            }
            match = _matchService.FindById(id);
            if (match != null)
            {
                matchFromBody.Id = id;
                if(matchFromBody.IdChampionship == "null")
                {
                    matchFromBody.IdChampionship = "000000000000000000000000";
                }
                matchFromBody = _matchService.Update(match.Id, matchFromBody);
                return "Match Updated";
            }
            return "Error to Update Match";
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Match match = default(Match);
            if(id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId incorect!!");
            }
            match = _matchService.FindById(id);
            if (match != null)
            {
                _matchService.Remove(id);
                return Ok("Match Removed");
            }
            return Ok("Error in Remove Match");
        }
    }
}
