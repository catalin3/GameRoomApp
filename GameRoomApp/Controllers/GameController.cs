﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Service.GameService;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace GameRoomApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {

        private const int NUMBER_OF_OBJID_CHARACTERS = 24;
        private ObjectId NULL_OBJECT_ID = ObjectId.Parse("000000000000000000000000");

        private readonly IGameService _gameService;

        public GameController(IGameService gameService)
        {
            this._gameService = gameService;
        }

        // GET: api/Game
        [HttpGet]
        public IActionResult Get([FromQuery] string Id, [FromQuery] string Name, [FromQuery] string Type)
        {
            if(Id != null) { return Ok(_gameService.FindById(Id)); }
            if(Name != null && Type != null) { return Ok(_gameService.FindByNameAndType(Name, Type)); }
            if (Name != null && Type == null) { return Ok(_gameService.FindByName(Name)); }
            return Ok(_gameService.FindAll());
        }

        [HttpPost]
        public IActionResult Post([FromQuery] Game gameFromBody)
        {
            Game game = default(Game);
            game = _gameService.Insert(gameFromBody);
            if (game != null)
            {
                return Ok(game);
            }
            return Ok(game);
        }

        // PUT: api/Game/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] Game gameFromBody)
        {
            Game game = default(Game);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId Incorect");
            }
            string objId = ObjectId.Parse(id).ToString();
            game = _gameService.FindById(objId);
            if (game != null)
            {
                gameFromBody.Id = objId;
                gameFromBody = _gameService.Update(game.Id, gameFromBody);
                return Ok("Game Updated");
            }
            return Ok("Error to Update Game");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Game game = default(Game);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId incorect!!");
            }
            game = _gameService.FindById(id);
            if (game != null)
            {
                _gameService.Remove(id);
                return Ok("Game Removed");
            }
            return Ok("Error in Game Remove");
        }
    }
}
