﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameRoomApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            this._userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Authenticate([FromBody]Player userParam)
        {
            var user = _userService.Authenticate(userParam.Nickname, userParam.Password);
            if (user == null)
            {
                return BadRequest(new { message = "Nickname or Password is incorect" });
            }
            return Ok(user);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var users = _userService.FindAll();
            return Ok(users);
        }
    }
}
