﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Service.RuleServicePack;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace GameRoomApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RuleController : ControllerBase
    {
        private const int NUMBER_OF_OBJID_CHARACTERS = 24;
        private ObjectId NULL_OBJECT_ID = ObjectId.Parse("000000000000000000000000");

        private readonly IRuleService _ruleService;
        public RuleController(IRuleService ruleService)
        {
            this._ruleService = ruleService;
        }

        // GET: api/Rule
        [HttpGet]
        public IActionResult Get([FromQuery] string Game, [FromQuery] string Type)
        {
            if(Game != null && Type != null) { return Ok(_ruleService.FindByGameAndType(Game, Type)); }
            if (Game != null && Type == null) { return Ok(_ruleService.FindByGame(Game)); }
            return Ok(_ruleService.FindAll());
        }

        // GET: api/Rule/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] Rule ruleFromBody)
        {
            Rule rule = default(Rule);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId Incorect");
            }
            string objId = ObjectId.Parse(id).ToString();
            rule = _ruleService.FindById(objId);
            if (rule != null)
            {
                ruleFromBody.Id = objId;
                ruleFromBody = _ruleService.Update(rule.Id, ruleFromBody);
                return Ok("Rule Updated");
            }
            return Ok("Error to Update Rule");
        }

        // POST: api/Rule
        [HttpPost]
        public IActionResult Post([FromBody] Rule ruleFromBody)
        {
            Rule rule = default(Rule);
            rule = _ruleService.Insert(ruleFromBody);
            if (rule != null)
            {
                return Ok(rule);
            }
            return Ok(rule);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Rule rule = default(Rule);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId incorect!!");
            }
            rule = _ruleService.FindById(id);
            if (rule != null)
            {
                _ruleService.Remove(id);
                return Ok("Rule Removed");
            }
            return Ok("Error in Rule Remove");
        }
    }
}
