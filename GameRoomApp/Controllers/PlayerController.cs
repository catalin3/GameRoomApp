﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;



namespace GameRoomApp.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private const int NUMBER_OF_OBJID_CHARACTERS = 24;
        private ObjectId NULL_OBJECT_ID = ObjectId.Parse("000000000000000000000000");

        private readonly IPlayerService _playerService;

        public PlayerController(IPlayerService playerService)
        {
            this._playerService = playerService;
        }

        // GET: api/Player
        [HttpGet]
        public IActionResult Get([FromQuery] string Property, [FromQuery] string Sort, [FromQuery] string PageNumber, [FromQuery] string PageSize, [FromQuery] string Count,/* [FromQuery] string Id,*/ [FromQuery] string Nickname, [FromQuery] string IdChampionship, [FromQuery] string Table)
        {
            if (Count != null)
            {
                return Ok(_playerService.GetNumberOfPlayers());
            }
            if (PageNumber != null && PageSize != null && Property != null && Sort != null)
            {
                var pageNumber = Int32.Parse(PageNumber);
                var pageSize = Int32.Parse(PageSize);
                return Ok(_playerService.PaginationFindAll(pageNumber, pageSize, Property, Sort));
            }
            if (Nickname != null) { return Ok(_playerService.FindByNickname(Nickname)); }
            //if (Id != null) { return Ok(_playerService.FindById(Id)); }
            if (Table != null && IdChampionship != null) { return Ok(_playerService.GetChampionshipState(IdChampionship)); }
            return Ok(_playerService.FindAll());
        }

        // POST: api/Player
        [HttpPost]
        public string Post([FromBody] Player value)
        {
            Player player = default(Player);
            player = _playerService.Insert(value);
            if (player != null)
            {
                return "Game added!!";
            }
            return "Error to add Game!!";
        }

        // PUT: api/Player/5
        [HttpPut("{id}")]
        public string Put(string id, [FromBody] Player value)
        {
            Player player = default(Player);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return "ObjectId incorect!!";
            }
            //ObjectId oid = ObjectId.Parse(id);
            player = _playerService.FindById(id);
            if (player != null)
            {
                value.Id = id;
                player = _playerService.Update(player.Id, value);
                return "Player Updated";
            }
            return "Error in Player Update";
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public string Delete(string id)
        {
            Player player = default(Player);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return "ObjectId incorect!!";
            }
            player = _playerService.FindById(id);
            if (player != null)
            {
                _playerService.Remove(id);
                return "Player Removed";
            }
            return "Error in Player Remove";
        }
        
    }
}
