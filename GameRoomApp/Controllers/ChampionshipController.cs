﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace GameRoomApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChampionshipController : ControllerBase
    {
        private const int NUMBER_OF_OBJID_CHARACTERS = 24;
        private readonly IChampionshipService _championshipService;

        public ChampionshipController(IChampionshipService championshipService)
        {
            this._championshipService = championshipService;
        }

        // GET: api/Match
        [HttpGet]
        public IActionResult Get([FromQuery] string Id, [FromQuery] string Name, [FromQuery] string Type, [FromQuery] string Status)
        {
            if (Id != null) { return Ok(_championshipService.FindById(Id)); }
            if (Name != null) { return Ok(_championshipService.FindByName(Name)); }
            if (Name != null && Type != null) { return Ok(_championshipService.FindByNameAndType(Name, Type));  }
            if(Status == "Ongoing")
            {
                return Ok(_championshipService.FindByStatus(Championship.Status.Ongoing));
            }
            if (Status == "End")
            {
                return Ok(_championshipService.FindByStatus(Championship.Status.End));
            }

            return Ok(_championshipService.FindAll());
        }

        // POST: api/Championship
        [HttpPost]
        public IActionResult Post([FromBody] Championship championship)
        {
            Championship champ = default(Championship);
            champ = _championshipService.Insert(championship);
            if (champ != null)
            {
                return Ok("Championship Added!!\nPrepare for the Games!!");
            }
            return Ok("Error to add Championship!!");
            
        }

        // PUT: api/Championship/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] Championship championship)
        {
            Championship champ = default(Championship);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId incorect");
            }
            //ObjectId oid = ObjectId.Parse(id);
            champ = _championshipService.FindById(id);
            if (champ != null)
            {
                championship.Id = id;
                champ = _championshipService.Update(champ.Id, championship);
                return Ok("Championship Updated");
            }
            return Ok("Error to Update Championship");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            Championship championship = default(Championship);
            if (id.Length != NUMBER_OF_OBJID_CHARACTERS)
            {
                return Ok("ObjectId incorect!!");
            }
            championship = _championshipService.FindById(id);
            if (championship != null)
            {
                _championshipService.Remove(id);
                return Ok("Championship Removed");
            }
            return Ok("Error in Championship Remove");
        }
    }
}
