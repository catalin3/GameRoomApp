﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Repository.ImageRepo;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using GameRoomApp.DataLayer.Repository.RuleRepo;
using GameRoomApp.DataLayer.Service;
using GameRoomApp.DataLayer.Service.GameService;
using GameRoomApp.DataLayer.Service.GameServicePack;
using GameRoomApp.DataLayer.Service.ImageServicePack;
using GameRoomApp.DataLayer.Service.RuleServicePack;
using GameRoomApp.DataLayer.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;


namespace GameRoomApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.Configure<Settings>(settings =>
            {
                settings.ConnectionString = Configuration.GetSection("MongoDB:ConnectionString").Value;
                settings.DatabaseName = Configuration.GetSection("MongoDB:Database").Value;
            });

            services.AddTransient<IContext<Player>, PlayerContext>();
            services.AddTransient<IPlayerRepository, PlayerRepository>();
            services.AddTransient<IPlayerService, PlayerService>();

            services.AddTransient<IContext<Championship>, ChampionshipContext>();
            services.AddTransient<IChampionshipRepository, ChampionshipRepository>();
            services.AddTransient<IChampionshipService, ChampionshipService>();

            services.AddTransient<IContext<Match>, MatchContext>();
            services.AddTransient<IMatchRepository, MatchRepository>();
            services.AddTransient<IMatchService, MatchService>();

            services.AddTransient<IContext<Game>, GameContext>();
            services.AddTransient<IGameRepository, GameRepository>();
            services.AddTransient<IGameService, GameService>();

            services.AddTransient<IContext<ImageMoment>, ImageContext>();
            services.AddTransient<IImageRepository, ImageRepository>();
            services.AddTransient<IImageService, ImageService>();

            services.AddTransient<IContext<Rule>, RuleContext>();
            services.AddTransient<IRuleRepository, RuleRepository>();
            services.AddTransient<IRuleService, RuleService>();

            //---------------------------------------------------------------------------------------------
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            //configure jwt authtntication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // configure DI for application services
            services.AddScoped<IUserService, UserService>();
            //
            //---------------------------------------------------------------------------------------------

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials();
            });
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
