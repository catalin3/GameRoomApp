﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service
{
    public interface IUserService : IService<Player>
    {
        Player Authenticate(string nickname, string password);
        IEnumerable<Player> GetAll();
    }
}
