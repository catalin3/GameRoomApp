﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace GameRoomApp.DataLayer.Service
{
    public class UserService : IUserService
    {
        private readonly IPlayerRepository _playerRepository;

        private readonly AppSettings _appSettings;

        public UserService(IPlayerRepository playerRepository, IOptions<AppSettings> appSettings)
        {
            this._playerRepository = playerRepository;
            this._appSettings = appSettings.Value;
        }

        public Player Authenticate(string nickname, string password)
        {
            var user = _playerRepository.FindAll().SingleOrDefault(x => x.Nickname == nickname && x.Password == password);
            if (user == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            user.Password = null;

            return user;
        }

        public IEnumerable<Player> FindAll()
        {
            return _playerRepository.FindAll();
        }

        public Player FindById(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Player> GetAll()
        {
            return this.FindAll().Select(x =>
            {
                x.Password = null;
                return x;
            });
        }

        public Player Insert(Player t)
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public Player Update(string id, Player t)
        {
            throw new NotImplementedException();
        }
    }
}
