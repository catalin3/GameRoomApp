﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Repository.ImageRepo;

namespace GameRoomApp.DataLayer.Service.ImageServicePack
{
    public class ImageService : IImageService
    {
        private readonly IImageRepository _imageRepository;
        private readonly IGameRepository _gameRepository;

        public ImageService(IImageRepository imageRepository, IGameRepository gameRepository)
        {
            this._imageRepository = imageRepository;
            this._gameRepository = gameRepository;
        }

        public IEnumerable<ImageMoment> FindAll()
        {
            return _imageRepository.FindAll();
        }

        public IEnumerable<ImageMoment> FindByGame(string game)
        {
            return _imageRepository.FindByGame(game);
        }

        public ImageMoment FindById(string id)
        {
            return _imageRepository.FindById(id);
        }

        private Boolean ExistGameInList(List<string> names, string game)
        {
            foreach(string name in names)
            {
                if(name == game)
                {
                    return true;
                }
            }
            return false;
        }

        public IEnumerable<string> FindGamesName()
        {
            var moments = _imageRepository.FindAll();
            //var games = _gameRepository.FindAll();
            List<string> names = new List<string>();
            foreach (ImageMoment image in moments)
            {
                if(!ExistGameInList(names,image.Game))
                {
                    names.Add(image.Game);
                }
                
            }
            return names;
        }

        public ImageMoment Insert(ImageMoment t)
        {
            return _imageRepository.Insert(t);
        }

        public void Remove(string id)
        {
            _imageRepository.Remove(id);
        }

        public ImageMoment Update(string id, ImageMoment t)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ImageMoment> PaginationFindAll(int pageNumber, int pageSize)
        {
            return _imageRepository.PaginationFindAll(pageNumber, pageSize);
        }

        public int GetNumberOfImages()
        {
            return _imageRepository.GetNumberOfImages();
        }

        public IEnumerable<ImageMoment> GetLast10()
        {
            var last10 = new List<ImageMoment>();
            if (GetNumberOfImages() < 10) { return FindAll(); }
            int pages = GetNumberOfImages() / 10;
            pages--;
            if (GetNumberOfImages() % 10 != 0) { pages++; }
            var pageOfImages = _imageRepository.PaginationFindAll(pages, 10);

            foreach (var image in pageOfImages)
            {
                last10.Add(image);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            var pageOfImages2 = _imageRepository.PaginationFindAll(pages - 1, 10);

            foreach (var image in pageOfImages2)
            {
                last10.Add(image);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            return last10;
        }
    }
}
