﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service.ImageServicePack
{
    public interface IImageService: IService<ImageMoment>
    {
        IEnumerable<string> FindGamesName();
        IEnumerable<ImageMoment> FindByGame(string game);
        IEnumerable<ImageMoment> PaginationFindAll(int pageNumber, int pageSize);
        int GetNumberOfImages();
        IEnumerable<ImageMoment> GetLast10();
    }
}
