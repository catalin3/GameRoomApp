﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RuleRepo;

namespace GameRoomApp.DataLayer.Service.RuleServicePack
{
    public class RuleService : IRuleService
    {
        private readonly IRuleRepository _ruleRepository;
        public RuleService(IRuleRepository ruleRepository)
        {
            this._ruleRepository = ruleRepository;
        }

        public IEnumerable<Rule> FindAll()
        {
            return _ruleRepository.FindAll();
        }

        public IEnumerable<Rule> FindByGame(string game)
        {
            return _ruleRepository.FindByGame(game);
        }

        public Rule FindByGameAndType(string game, string type)
        {
            return _ruleRepository.FindByGameAndType(game, type);
        }

        public Rule FindById(string id)
        {
            return _ruleRepository.FindById(id);
        }

        public Rule Insert(Rule t)
        {
            return _ruleRepository.Insert(t);
        }

        public void Remove(string id)
        {
            _ruleRepository.Remove(id);
        }

        public Rule Update(string id, Rule t)
        {
            return _ruleRepository.Update(id, t);
        }
    }
}
