﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Validator;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service
{
    public class PlayerService:IPlayerService
    {
        private IPlayerRepository _playerRepository;
        private IMatchRepository _matchRepository;
        private IChampionshipRepository _championshipRepository;

        public PlayerService(IPlayerRepository playerRepository, IMatchRepository matchRepository, IChampionshipRepository championshipRepository)
        {
            this._playerRepository = playerRepository;
            this._matchRepository = matchRepository;
            this._championshipRepository = championshipRepository;
        }

        

        public IEnumerable<Player> FindAll()
        {
            return _playerRepository.FindAll();
        }

        public Player FindByNickname(string nickname)
        {
            return _playerRepository.FindByNickname(nickname);
        }

        public Player Insert(Player t)
        {
            return _playerRepository.Insert(t);
        }

        public Player Update(string id, Player t)
        {
            return _playerRepository.Update(id, t);
        }

        public void Remove(string id)
        {
            _playerRepository.Remove(id);
        }

        public Player FindById(string id)
        {
            return _playerRepository.FindById(id);
        }

        public void RemoveByNickname(string nickname)
        {
            Player player = FindByNickname(nickname);
            _playerRepository.Remove(player.Id);
        }

        public IEnumerable<Player> FindByStatus(string status)
        {
            return _playerRepository.FindByStatus(status);
        }

        public IEnumerable<Player> FindByExperience(int experience)
        {
            return _playerRepository.FindByExperience(experience);
        }

        private List<StandingEntity> initializeTable(List<String> participants)
        {
            List<StandingEntity> table = new List<StandingEntity>();
            foreach(string nickname in participants)
            {
                StandingEntity standingEntity = new StandingEntity();
                standingEntity.Nickname = nickname;
                standingEntity.Points = 0;
                standingEntity.Wins = 0;
                standingEntity.Draws = 0;
                standingEntity.Losses = 0;
                standingEntity.MatchesPlayed = 0;
                standingEntity.goalsFor = 0;
                standingEntity.goalsAgainst = 0;
                table.Add(standingEntity);
            }
            return table;
        }

        private void addScore(List<StandingEntity> table, Match match)
        {
            int index = table.FindIndex(el => el.Nickname == match.HomeTeam[0]);
            table.ElementAt(index).MatchesPlayed += 1;
            if (match.ScoreHomeTeam[0] > match.ScoreAwayTeam[0])
            {
                table.ElementAt(index).Points += 3;
                table.ElementAt(index).Wins += 1;
                table.ElementAt(index).goalsFor += match.ScoreHomeTeam[0];
                table.ElementAt(index).goalsAgainst += match.ScoreAwayTeam[0];

                index = table.FindIndex(el => el.Nickname == match.AwayTeam[0]);
                table.ElementAt(index).Losses += 1;
                table.ElementAt(index).goalsAgainst += match.ScoreHomeTeam[0];
                table.ElementAt(index).goalsFor += match.ScoreAwayTeam[0];
                table.ElementAt(index).MatchesPlayed += 1;
            }

            if (match.ScoreHomeTeam[0] < match.ScoreAwayTeam[0])
            {
                table.ElementAt(index).Losses += 1;
                table.ElementAt(index).goalsFor += match.ScoreHomeTeam[0];
                table.ElementAt(index).goalsAgainst += match.ScoreAwayTeam[0];

                index = table.FindIndex(el => el.Nickname == match.AwayTeam[0]);
                table.ElementAt(index).Points += 3;
                table.ElementAt(index).Wins += 1;
                table.ElementAt(index).goalsAgainst += match.ScoreHomeTeam[0];
                table.ElementAt(index).goalsFor += match.ScoreAwayTeam[0];
                table.ElementAt(index).MatchesPlayed += 1;
            }

            if (match.ScoreHomeTeam[0] == match.ScoreAwayTeam[0])
            {
                table.ElementAt(index).Draws += 1;
                table.ElementAt(index).Points += 1;
                table.ElementAt(index).goalsFor += match.ScoreHomeTeam[0];
                table.ElementAt(index).goalsAgainst += match.ScoreAwayTeam[0];

                index = table.FindIndex(el => el.Nickname == match.AwayTeam[0]);
                table.ElementAt(index).Points += 1;
                table.ElementAt(index).Draws += 1;
                table.ElementAt(index).goalsAgainst += match.ScoreHomeTeam[0];
                table.ElementAt(index).goalsFor += match.ScoreAwayTeam[0];
                table.ElementAt(index).MatchesPlayed += 1;
            }
        }

        public IEnumerable<StandingEntity> GetChampionshipState(string idChampionship)
        {
            Championship championship = _championshipRepository.FindById(idChampionship);
            List<StandingEntity> table = initializeTable(championship.Participants);
            List<Match> championshipMatches = _matchRepository.FindByChampionship(idChampionship).ToList();

            foreach(Match match in championshipMatches)
            {
                if(match.ScoreHomeTeam != null && match.AwayTeam != null)
                {
                    addScore(table, match);
                }
            }/*
            table.Sort(delegate (StandingEntity x, StandingEntity y)
            {
                if (x.Points == null && y.Points == null) return 0;
                else if (x.Points == null) return -1;
                else if (y.Points == null) return 1;
                else return x.Points.CompareTo(y.Points);
            });*/
            return table.OrderByDescending(e => e.Points);
        }

        public IEnumerable<Player> PaginationFindAll(int pageNumber, int pageSize, string property, string sort)
        {
            return _playerRepository.PaginationFindAll(pageNumber, pageSize, property, sort);
        }

        public int GetNumberOfPlayers()
        {
            return _playerRepository.GetNumberOfPlayers();
        }
    }
}
