﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service
{
    public interface IPlayerService:IService<Player>
    {
        Player FindByNickname(string nickname);
        IEnumerable<Player> FindByStatus(string status);
        IEnumerable<Player> FindByExperience(int experience);
        void RemoveByNickname(string nickname);

        IEnumerable<StandingEntity> GetChampionshipState(string idChampionship);

        IEnumerable<Player> PaginationFindAll(int pageNumber, int pageSize, string property, string sort);
        int GetNumberOfPlayers();
    }
}
