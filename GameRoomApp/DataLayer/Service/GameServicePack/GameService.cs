﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Service.GameService;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service.GameServicePack
{
    public class GameService : IGameService
    {
        private readonly IGameRepository _gameRepository;

        public GameService(IGameRepository gameRepository)
        {
            this._gameRepository = gameRepository;
        }

        public IEnumerable<Game> FindAll()
        {
            return _gameRepository.FindAll();
        }

        public Game FindById(string id)
        {
            return _gameRepository.FindById(id);
        }

        public IEnumerable<Game> FindByName(string name)
        {
            return _gameRepository.FindByName(name);
        }

        public Game FindByNameAndType(string name, string type)
        {
            return _gameRepository.FindByNameAndType(name, type);
        }

        public Game Insert(Game t)
        {
            return _gameRepository.Insert(t);
        }

        public void Remove(string id)
        {
            _gameRepository.Remove(id);
        }

        public Game Update(string id, Game t)
        {
            return _gameRepository.Update(id, t);
        }
    }
}
