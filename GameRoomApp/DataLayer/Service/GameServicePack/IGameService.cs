﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service.GameService
{
    public interface IGameService:IService<Game>
    {
        Game FindByNameAndType(string name, string type);
        IEnumerable<Game> FindByName(string name);
    }
}
