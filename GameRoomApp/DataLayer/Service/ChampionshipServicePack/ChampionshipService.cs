﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using MongoDB.Bson;

namespace GameRoomApp.DataLayer.Service
{
    public class ChampionshipService : IChampionshipService
    {
        private readonly IChampionshipRepository _championshipRepository;
        private readonly IMatchRepository _matchRepository;
        private readonly IGameRepository _gameRepository;

        public ChampionshipService(IChampionshipRepository championshipRepository, IMatchRepository matchRepository, IGameRepository gameRepository)
        {
            this._championshipRepository = championshipRepository;
            this._matchRepository = matchRepository;
            this._gameRepository = gameRepository;
        }
        public IEnumerable<Championship> FindAll()
        {
            return _championshipRepository.FindAll();
        }

        public Championship FindById(string id)
        {
            return _championshipRepository.FindById(id);
        }

        public IEnumerable<Championship> FindByName(string name)
        {
            return _championshipRepository.FindByName(name);
        }

        public IEnumerable<Championship> FindByPlayerNickname(string nickname)
        {
            return _championshipRepository.FindByPlayerNickname(nickname);
        }

        public IEnumerable<Championship> FindByStatus(Championship.Status status)
        {
            return _championshipRepository.FindByStatus(status);
        }

        public IEnumerable<Championship> FindByType(string type)
        {
            return _championshipRepository.FindByType(type);
        }

        public void AddGamesFromChampionship(Championship championship)
        {
            for(int i = 0; i<championship.Participants.Count-1; i++)
            {
                for(int j = i + 1; j < championship.Participants.Count; j++)
                {
                    Game game = _gameRepository.FindByNameAndType(championship.Name, championship.Type);
                    Match match = new Match
                    {
                        IdGame = game.Id,
                        HomeTeam = new List<string> { championship.Participants.ElementAt(i) },
                        AwayTeam = new List<string> { championship.Participants.ElementAt(j) },
                        IdChampionship = championship.Id.ToString()
                    };
                    _matchRepository.Insert(match);
                }
            }
        }

        public Championship Insert(Championship t)
        {
            Championship championship = _championshipRepository.Insert(t);
            if (t.Participants.Count > 2)
            {
                AddGamesFromChampionship(championship);
            }
            return championship;
        }

        public void Remove(string id)
        {
            _championshipRepository.Remove(id);
        }

        public Championship Update(string id, Championship t)
        {
            if (t.Participants.Count > 2)
            {
                AddGamesFromChampionship(t);
            }
            return _championshipRepository.Update(id, t);
        }

        public IEnumerable<Championship> FindByNameAndType(string name, string type)
        {
            return _championshipRepository.FindByNameAndType(name, type);
        }
    }
}
