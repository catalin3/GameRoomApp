﻿using GameRoomApp.DataLayer.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service
{
    public interface IMatchService:IService<Match>
    {
        IEnumerable<Match> FindByNameAndType(string name, string type);
        IEnumerable<Match> FindByChampionship(string idChampionship);
        IEnumerable<Match> PaginationFindAll(int pageNumber, int pageSize);
        IEnumerable<Match> GetLast10();
        int GetNumberOfMatches();
    }
}
