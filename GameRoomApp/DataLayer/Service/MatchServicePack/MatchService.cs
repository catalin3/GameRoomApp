﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using MongoDB.Bson;

namespace GameRoomApp.DataLayer.Service
{
    public class MatchService : IMatchService
    {
        private const string NULL_CHAMPIONSHIP = "000000000000000000000000";
        private readonly IMatchRepository _matchRepository;
        private readonly IGameRepository _gameRepository;
        private readonly IChampionshipRepository _championshipRepository;
        private readonly IPlayerRepository _playerRepository;

        public MatchService(IPlayerRepository playerRepository, IMatchRepository matchRepository, IGameRepository gameRepository, IChampionshipRepository championshipRepository)
        {
            this._playerRepository = playerRepository;
            this._matchRepository = matchRepository;
            this._gameRepository = gameRepository;
            this._championshipRepository = championshipRepository;
        }

        public IEnumerable<Match> FindAll()
        {
            return _matchRepository.FindAll();
        }

        public IEnumerable<Match> FindByChampionship(string idChampionship)
        {
            return _matchRepository.FindByChampionship(idChampionship);
        }

        public Match FindById(string id)
        {
            return _matchRepository.FindById(id);
        }

        public IEnumerable<Match> FindByNameAndType(string name, string type)
        {
            Game game = _gameRepository.FindByNameAndType(name,type);
            return FindByIdGame(game.Id.ToString());
        }

        public IEnumerable<Match> FindByIdGame(string id)
        {
            return _matchRepository.FindByIdGame(id);
        }

        private void addPlayerExperience(List<string> winers, List<string> losers, Boolean equal)
        {
            int winnerPoints = 15;
            int loserPoints = 1;
            if (equal)
            {
                winnerPoints = 8;
                loserPoints = 8;

            }
            foreach (var nickname in winers)
            {
                Player newPlayer = _playerRepository.FindByNickname(nickname);
                newPlayer.Experience += winnerPoints;
                Player updated = _playerRepository.Update(newPlayer.Id, newPlayer);
            }
            foreach (var nickname in losers)
            {
                Player newPlayer = _playerRepository.FindByNickname(nickname);
                newPlayer.Experience += loserPoints;
                Player updated = _playerRepository.Update(newPlayer.Id, newPlayer);
            }
        }

        public Match Insert(Match t)
        {
            if (t.ScoreAwayTeam[0] != null)
            {
                if (t.ScoreHomeTeam[0] > t.ScoreAwayTeam[0])
                {
                    addPlayerExperience(t.HomeTeam, t.AwayTeam, false);
                }
                if (t.ScoreHomeTeam[0] < t.ScoreAwayTeam[0])
                {
                    addPlayerExperience(t.AwayTeam, t.HomeTeam, false);
                }
                if (t.ScoreHomeTeam[0] == t.ScoreAwayTeam[0])
                {
                    addPlayerExperience(t.AwayTeam, t.HomeTeam, true);
                }
            }
            return _matchRepository.Insert(t);
        }


        public void Remove(string id)
        {
            _matchRepository.Remove(id);
        }

        private Boolean checkChampionshipStatus(string idChampionship)
        {
            List<Match> matches = _matchRepository.FindByChampionship(idChampionship).ToList();
            foreach(Match match in matches)
            {
                if(match.ScoreHomeTeam == null)
                {
                    return false;
                }
            }
            return true;
        }

        public Match Update(string id, Match t)
        {
            if(t.IdChampionship != NULL_CHAMPIONSHIP)
            {
                if (checkChampionshipStatus(t.IdChampionship))
                {
                    Championship championship = _championshipRepository.FindById(t.IdChampionship);
                    if (championship.CurentStatus.Equals(Championship.Status.Ongoing))
                    {
                        championship.CurentStatus = Championship.Status.End;
                        _championshipRepository.Update(t.IdChampionship, championship);
                    }
                }
            }
            return _matchRepository.Update(id, t);
        }

        public IEnumerable<Match> PaginationFindAll(int pageNumber, int pageSize)
        {
            return _matchRepository.PaginationFindAll(pageNumber, pageSize);
        }

        public IEnumerable<Match> GetLast10()
        {
            var last10 = new List<Match>();
            if(GetNumberOfMatches() < 10) { return FindAll(); }
            int pages = GetNumberOfMatches() / 10;
            pages--;
            if(GetNumberOfMatches() % 10 != 0) { pages++; }
            var pageOfMatches = _matchRepository.PaginationFindAll(pages, 10);

            foreach(var match in pageOfMatches)
            {
                last10.Add(match);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            var pageOfMatches2 = _matchRepository.PaginationFindAll(pages-1, 10);

            foreach (var match in pageOfMatches2)
            {
                last10.Add(match);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            return last10;
        }

        public int GetNumberOfMatches()
        {
            return _matchRepository.GetNumberOfMatches();
        }
    }
}
