﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Service
{
    public interface IService<T>
    {
        IEnumerable<T> FindAll();

        T Insert(T t);

        T Update(string id, T t);

        void Remove(string id);

        T FindById(string id);
    }
}
