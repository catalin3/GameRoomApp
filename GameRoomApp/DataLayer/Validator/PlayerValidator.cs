﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Validator
{
    public class PlayerValidator : IValidator<Player>
    {
        public void Validate(Player t)
        {
            string errMsg = "";

            if (string.IsNullOrEmpty(t.Nickname))
                errMsg += "Insert the Nickname\n";

            if (errMsg != "")
                throw new ValidatorException(errMsg);
        }
    }
}
