﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Validator
{
    public class ValidatorException:ApplicationException
    {
        public ValidatorException() : base() { }
        public ValidatorException(string m) : base(m) { }
    }
}
