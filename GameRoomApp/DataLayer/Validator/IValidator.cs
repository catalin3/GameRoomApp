﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Validator
{
    public interface IValidator<T>
    {
        void Validate(T t);
    }
}
