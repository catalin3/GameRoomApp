﻿using GameRoomApp.DataLayer.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository.RepoUtils
{
    public class ImageContext : IContext<ImageMoment>
    {
        private readonly IMongoDatabase _database;

        public ImageContext(IOptions<Settings> options)
        {
            MongoClient client = new MongoClient(options.Value.ConnectionString);
            this._database = client.GetDatabase(options.Value.DatabaseName);
        }

        public IMongoCollection<ImageMoment> Entities
        {
            get
            {
                return _database.GetCollection<ImageMoment>("ImageMoments");
            }
        }
    }
}
