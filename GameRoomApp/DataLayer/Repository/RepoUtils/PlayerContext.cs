﻿using GameRoomApp.DataLayer.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository.RepoUtils
{
    public class PlayerContext : IContext<Player>
    {
        private readonly IMongoDatabase _database;

        public PlayerContext(IOptions<Settings> options)
        {
            MongoClient client = new MongoClient(options.Value.ConnectionString);
            this._database = client.GetDatabase(options.Value.DatabaseName);
        }

        public IMongoCollection<Player> Entities
        {
            get
            {
                return _database.GetCollection<Player>("Players");
            }
        }
    }
}
