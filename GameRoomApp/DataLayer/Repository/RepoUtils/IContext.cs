﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository.RepoUtils
{
    public interface IContext<T>
    {
        IMongoCollection<T> Entities { get; }
    }
}
