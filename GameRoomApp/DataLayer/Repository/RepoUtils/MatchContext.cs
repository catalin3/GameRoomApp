﻿using GameRoomApp.DataLayer.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository.RepoUtils
{
    public class MatchContext : IContext<Match>
    {
        private readonly IMongoDatabase _database;

        public MatchContext(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            this._database = client.GetDatabase(options.Value.DatabaseName);
        }

        public IMongoCollection<Match> Entities
        {
            get
            {
                return _database.GetCollection<Match>("Matches");
            }
        }
    }
}
