﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository
{
    public interface IChampionshipRepository:IRepository<Championship>
    {
        IEnumerable<Championship> FindByName(string name);
        IEnumerable<Championship> FindByType(string type);
        IEnumerable<Championship> FindByStatus(Championship.Status status);
        IEnumerable<Championship> FindByPlayerNickname(string nickname);
        IEnumerable<Championship> FindByNameAndType(string name, string type);
    }
}
