﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GameRoomApp.DataLayer.Repository
{
    public class ChampionshipRepository : IChampionshipRepository
    {
        private readonly IContext<Championship> _championshipContext;

        public ChampionshipRepository(IContext<Championship> championshipContext)
        {
            this._championshipContext = championshipContext;
        }


        public IEnumerable<Championship> FindAll()
        {
            var emptyFilter = Builders<Championship>.Filter.Empty;
            return _championshipContext.Entities.FindSync(emptyFilter).ToList();
        }

        public Championship FindById(string id)
        {
            var filter = Builders<Championship>.Filter.Eq<string>(c => c.Id, id);
            return _championshipContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public IEnumerable<Championship> FindByName(string name)
        {
            var filter = Builders<Championship>.Filter.Eq<string>(c => c.Name, name);
            return _championshipContext.Entities.FindSync(filter).ToList();
        }

        public IEnumerable<Championship> FindByNameAndType(string name, string type)
        {
            var filter = Builders<Championship>.Filter.And(
                Builders<Championship>.Filter.Eq<string>(g => g.Name, name),
                Builders<Championship>.Filter.Eq<string>(g => g.Type, type)
                );
            return _championshipContext.Entities.FindSync(filter).ToList();
        }

        public IEnumerable<Championship> FindByPlayerNickname(string nickname)
        {
            var filter = Builders<Championship>.Filter.ElemMatch<string>(c => c.Participants, nickname);
            return _championshipContext.Entities.FindSync(filter).ToList();
        }

        public IEnumerable<Championship> FindByStatus(Championship.Status status)
        {
            var filter = Builders<Championship>.Filter.Eq<Championship.Status>(c => c.CurentStatus, status);
            return _championshipContext.Entities.FindSync(filter).ToList();
        }

        public IEnumerable<Championship> FindByType(string type)
        {
            var filter = Builders<Championship>.Filter.Eq<string>(c => c.Type, type);
            return _championshipContext.Entities.FindSync(filter).ToList();
        }

        public Championship Insert(Championship t)
        {
            _championshipContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public void Remove(string id)
        {
            var filter = Builders<Championship>.Filter.Eq<string>(c => c.Id, id);
            _championshipContext.Entities.DeleteOne(filter);
        }

        public Championship Update(string id, Championship t)
        {
            var filter = Builders<Championship>.Filter.Eq<string>(c => c.Id, id);
            _championshipContext.Entities.ReplaceOne(filter, t);
            return FindById(t.Id);
        }
    }
}
