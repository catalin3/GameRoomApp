﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository.ImageRepo
{
    public interface IImageRepository:IRepository<ImageMoment>
    {
        IEnumerable<ImageMoment> FindByGame(string game);
        IEnumerable<ImageMoment> PaginationFindAll(int pageNumber, int pageSize);
        int GetNumberOfImages();
    }
}
