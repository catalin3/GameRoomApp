﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using MongoDB.Driver;

namespace GameRoomApp.DataLayer.Repository.ImageRepo
{
    public class ImageRepository : IImageRepository
    {
        private readonly IContext<ImageMoment> _imageContext;

        public ImageRepository(IContext<ImageMoment> imageContext)
        {
            this._imageContext = imageContext;
        }
       

        public IEnumerable<ImageMoment> FindAll()
        {
            var emptyFilter = Builders<ImageMoment>.Filter.Empty;
            return _imageContext.Entities.FindSync(emptyFilter).ToList();
        }

        public IEnumerable<ImageMoment> FindByGame(string game)
        {
            var filter = Builders<ImageMoment>.Filter.Eq<string>(c => c.Game, game);
            return _imageContext.Entities.FindSync(filter).ToList();
        }

        public ImageMoment FindById(string id)
        {
            var filter = Builders<ImageMoment>.Filter.Eq<string>(c => c.Id, id);
            return _imageContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public ImageMoment Insert(ImageMoment t)
        {
            _imageContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public IEnumerable<ImageMoment> PaginationFindAll(int pageNumber, int pageSize)
        {
            var emptyFilter = Builders<ImageMoment>.Filter.Empty;
            return _imageContext.Entities.Find(emptyFilter).Limit(pageSize).Skip(pageNumber * pageSize).ToList();
        }

        public void Remove(string id)
        {
            var filter = Builders<ImageMoment>.Filter.Eq<string>(g => g.Id, id);
            _imageContext.Entities.DeleteOne(filter);
        }

        public ImageMoment Update(string id, ImageMoment t)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfImages()
        {
            var emptyFilter = Builders<ImageMoment>.Filter.Empty;
            return _imageContext.Entities.FindSync(emptyFilter).ToList().Count();
        }
    }
}
