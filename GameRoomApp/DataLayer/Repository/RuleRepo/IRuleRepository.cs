﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository.RuleRepo
{
    public interface IRuleRepository:IRepository<Rule>
    {
        Rule FindByGameAndType(string game, string type);
        IEnumerable<Rule> FindByGame(string game);
    }
}
