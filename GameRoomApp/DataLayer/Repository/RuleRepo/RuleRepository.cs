﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using MongoDB.Driver;

namespace GameRoomApp.DataLayer.Repository.RuleRepo
{
    public class RuleRepository : IRuleRepository
    {
        private readonly IContext<Rule> _ruleContext;

        public RuleRepository(IContext<Rule> ruleContext)
        {
            this._ruleContext = ruleContext;
        }

        public IEnumerable<Rule> FindAll()
        {
            var emptyFilter = Builders<Rule>.Filter.Empty;
            return _ruleContext.Entities.FindSync(emptyFilter).ToList();
        }

        public IEnumerable<Rule> FindByGame(string game)
        {
            var filter = Builders<Rule>.Filter.Eq<string>(g => g.Game, game);
            return _ruleContext.Entities.FindSync(filter).ToList();
        }


        public Rule FindByGameAndType(string game, string type)
        {
            var filter = Builders<Rule>.Filter.And(
                Builders<Rule>.Filter.Eq<string>(g => g.Game, game),
                Builders<Rule>.Filter.Eq<string>(g => g.Type, type)
                );
            return _ruleContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public Rule FindById(string id)
        {
            var filter = Builders<Rule>.Filter.Eq<string>(g => g.Id, id);
            return _ruleContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public Rule Insert(Rule t)
        {
            _ruleContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public void Remove(string id)
        {
            var filter = Builders<Rule>.Filter.Eq<string>(g => g.Id, id);
            _ruleContext.Entities.DeleteOne(filter);
        }

        public Rule Update(string id, Rule t)
        {
            var filter = Builders<Rule>.Filter.Eq<string>(g => g.Id, id);
            _ruleContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
