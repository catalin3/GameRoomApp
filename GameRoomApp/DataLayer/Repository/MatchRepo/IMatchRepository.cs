﻿using GameRoomApp.DataLayer.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository
{
    public interface IMatchRepository:IRepository<Match>
    {
        IEnumerable<Match> FindByChampionship(string idChampionship);
        IEnumerable<Match> FindByIdGame(string id);
        IEnumerable<Match> PaginationFindAll(int pageNumber, int pageSize);
        int GetNumberOfMatches();
    }
}
