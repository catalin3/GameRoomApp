﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository
{
    public class MatchRepository : IMatchRepository
    {
        private readonly IContext<Match> _matchContext;

        public MatchRepository(IContext<Match> matchContext)
        {
            this._matchContext = matchContext;
        }

        public IEnumerable<Match> FindAll()
        {
            var emptyFilter = Builders<Match>.Filter.Empty;
            return _matchContext.Entities.FindSync(emptyFilter).ToList();
        }

        public IEnumerable<Match> FindByChampionship(string idChampionship)
        {
            var filter = Builders<Match>.Filter.Eq<string>(g => g.IdChampionship, idChampionship);
            return _matchContext.Entities.FindSync(filter).ToList();
        }

        public Match FindById(string id)
        {
            var filter = Builders<Match>.Filter.Eq<string>(g => g.Id, id);
            return _matchContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public IEnumerable<Match> FindByIdGame(string id)
        {
            var filter = Builders<Match>.Filter.Eq<string>(g => g.IdGame, id);
            return _matchContext.Entities.FindSync(filter).ToList();
        }

        public Match Insert(Match t)
        {
            _matchContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public IEnumerable<Match> PaginationFindAll(int pageNumber, int pageSize)
        {
            var emptyFilter = Builders<Match>.Filter.Empty;
            return _matchContext.Entities.Find(emptyFilter).Limit(pageSize).Skip(pageNumber * pageSize).ToList();
        }

        public void Remove(string id)
        {
            var filter = Builders<Match>.Filter.Eq<string>(g => g.Id, id);
            _matchContext.Entities.DeleteOne(filter);
        }

        public Match Update(string id, Match t)
        {
            var filter = Builders<Match>.Filter.Eq<string>(g => g.Id, id);
            _matchContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }

        public int GetNumberOfMatches()
        {
            var emptyFilter = Builders<Match>.Filter.Empty;
            return _matchContext.Entities.FindSync(emptyFilter).ToList().Count();
        }
    }
}
