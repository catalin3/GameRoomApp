﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly IContext<Player> _playerContext;

        public PlayerRepository(IContext<Player> playerContext)
        {
            this._playerContext = playerContext;
        }

        public IEnumerable<Player> FindAll()
        {
            var emptyFilter = Builders<Player>.Filter.Empty;
            return _playerContext.Entities.FindSync(emptyFilter).ToList();
        }

        public IEnumerable<Player> FindByExperience(int experience)
        {
            var filter = Builders<Player>.Filter.Eq<int>(p => p.Experience, experience);
            return _playerContext.Entities.FindSync(filter).ToList();
        }

        public Player FindById(string id)
        {
            var filter = Builders<Player>.Filter.Eq<string>(p => p.Id, id);
            return _playerContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public Player FindByNickname(string nickname)
        {
            var filter = Builders<Player>.Filter.Eq<string>(p => p.Nickname, nickname);
            return _playerContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public IEnumerable<Player> FindByStatus(string status)
        {
            var filter = Builders<Player>.Filter.Eq<string>(p => p.Status, status);
            return _playerContext.Entities.FindSync(filter).ToList();
        }

        public int GetNumberOfPlayers()
        {
            var emptyFilter = Builders<Player>.Filter.Empty;
            return _playerContext.Entities.FindSync(emptyFilter).ToList().Count();
        }

        public Player Insert(Player t)
        {
            _playerContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public IEnumerable<Player> PaginationFindAll(int pageNumber, int pageSize, string property, string sort)
        {
            var emptyFilter = Builders<Player>.Filter.Empty;
            if (sort == "Descending")
            {
                if (property == "Experience") { return _playerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Experience).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Nickname") { return _playerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Nickname).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "FirstName") { return _playerContext.Entities.Find(emptyFilter).SortByDescending(p => p.FirstName).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "LastName") { return _playerContext.Entities.Find(emptyFilter).SortByDescending(p => p.LastName).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Status") { return _playerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Status).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Email") { return _playerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Email).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
            }
            else
            {
                if (property == "Experience") { return _playerContext.Entities.Find(emptyFilter).SortBy(p => p.Experience).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Nickname") { return _playerContext.Entities.Find(emptyFilter).SortBy(p => p.Nickname).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "FirstName") { return _playerContext.Entities.Find(emptyFilter).SortBy(p => p.FirstName).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "LastName") { return _playerContext.Entities.Find(emptyFilter).SortBy(p => p.LastName).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Status") { return _playerContext.Entities.Find(emptyFilter).SortBy(p => p.Status).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Email") { return _playerContext.Entities.Find(emptyFilter).SortBy(p => p.Email).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
            }
            return FindAll();
        }

        public void Remove(string id)
        {
            var filter = Builders<Player>.Filter.Eq<string>(g => g.Id, id);
            _playerContext.Entities.DeleteOne(filter);
        }

        public void RemoveByNickname(string nickname)
        {
            var filter = Builders<Player>.Filter.Eq<string>(g => g.Nickname, nickname);
            _playerContext.Entities.DeleteOne(filter);
        }

        public Player Update(string id, Player t)
        {
            var filter = Builders<Player>.Filter.Eq<string>(g => g.Id, id);
            _playerContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
