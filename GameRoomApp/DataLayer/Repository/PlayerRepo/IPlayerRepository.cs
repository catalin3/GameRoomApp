﻿using GameRoomApp.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository
{
    public interface IPlayerRepository:IRepository<Player>
    {
        Player FindByNickname(string nickname);
        IEnumerable<Player> FindByStatus(string status);
        IEnumerable<Player> FindByExperience(int experience);
        void RemoveByNickname(string nickname);
        int GetNumberOfPlayers();
        IEnumerable<Player> PaginationFindAll(int pageNumber, int pageSize, string property, string sort);
    }
}
