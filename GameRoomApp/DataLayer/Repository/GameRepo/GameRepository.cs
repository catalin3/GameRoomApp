﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository.RepoUtils;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GameRoomApp.DataLayer.Repository
{
    public class GameRepository : IGameRepository
    {
        private readonly IContext<Game> _gameContext;

        public GameRepository(IContext<Game> gameContext)
        {
            this._gameContext = gameContext;
        }

        public IEnumerable<Game> FindAll()
        {
            var emptyFilter = Builders<Game>.Filter.Empty;

            return _gameContext.Entities.FindSync(emptyFilter).ToList();
        }

        public Game FindById(string id)
        {
            var filter = Builders<Game>.Filter.Eq<string>(g => g.Id, id);
            return _gameContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public IEnumerable<Game> FindByName(string name)
        {
            var filter = Builders<Game>.Filter.Eq<string>(g => g.Name, name);
            return _gameContext.Entities.FindSync(filter).ToList();
        }

        public Game FindByNameAndType(string name, string type)
        {
            var filter = Builders<Game>.Filter.And(
                Builders<Game>.Filter.Eq<string>(g=>g.Name,name),
                Builders<Game>.Filter.Eq<string>(g=>g.Type,type)
                );
            return _gameContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public Game Insert(Game t)
        {
            _gameContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public void Remove(string id)
        {
            var filter = Builders<Game>.Filter.Eq<string>(g => g.Id, id);
            _gameContext.Entities.DeleteOne(filter);
        }

        public Game Update(string id, Game t)
        {
            var filter = Builders<Game>.Filter.Eq<string>(g => g.Id, id);
            _gameContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
