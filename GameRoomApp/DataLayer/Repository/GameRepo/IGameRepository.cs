﻿using GameRoomApp.DataLayer.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Repository
{
    public interface IGameRepository : IRepository<Game>
    {
        Game FindByNameAndType(string name, string type);
        IEnumerable<Game> FindByName(string name);
    }
}
