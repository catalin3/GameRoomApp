﻿using GameRoomApp.DataLayer.Models;
using GameRoomApp.DataLayer.Repository;
using GameRoomApp.DataLayer.Service;
using GameRoomApp.DataLayer.Service.GameService;
using GameRoomApp.DataLayer.Service.RuleServicePack;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Utils
{
    public class DatabasePopulator
    {
        public MongoClient client;
        public IMongoDatabase mongoDatabase;


        public List<Game> findAllGames = new List<Game>();
        public List<Player> findAllPlayers = new List<Player>();


        public DatabasePopulator()
        {
            client = new MongoClient();
            mongoDatabase = client.GetDatabase("GameRoomApp");
        }

        public void Populate()
        {
            AddPlayers();
            AddGames();
            AddChmapionships();
            AddMatches();
            AddRules();
        }

        public void AddPlayers()
        {
            IMongoCollection<Player> players = mongoDatabase.GetCollection<Player>("Players");
            Player player = new Player
            {
                FirstName = "Denis",
                LastName = "Denis",
                Nickname = "Denisuper",
                Email = "denis" + "@yahoo.com",
                Password = "denisuper",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Catalin",
                LastName = "Alba",
                Nickname = "Albatraozz",
                Email = "catalin_alba" + "@rocketmail.com",
                Password = "albatraozz",
                Status = "legendary",
                Experience = 1000000
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Ion",
                LastName = "Ionescu",
                Nickname = "Ionelu",
                Email = "ionion" + "@yahoo.com",
                Password = "ionelu",
                Status = "rocky",
                Experience = 1000
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Dan",
                LastName = "Ionescu",
                Nickname = "dandan",
                Email = "daniel" + "@yahoo.com",
                Password = "dandan",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Vlad",
                LastName = "Mihai",
                Nickname = "Gamerul",
                Email = "vlad_mihai" + "@yahoo.com",
                Password = "gamerul",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Edi",
                LastName = "Pop",
                Nickname = "Edipop",
                Email = "ediedi" + "@yahoo.com",
                Password = "edipop",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Alin",
                LastName = "Rus",
                Nickname = "Alalin",
                Email = "alin" + "@yahoo.com",
                Password = "alalin",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Emilia",
                LastName = "Pop",
                Nickname = "Emilia123",
                Email = "popemilia" + "@yahoo.com",
                Password = "emilia123",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Andreea",
                LastName = "Stan",
                Nickname = "Andreiuta",
                Email = "deea" + "@yahoo.com",
                Password = "andreiuta",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Maria",
                LastName = "Luca",
                Nickname = "Marya",
                Email = "marya" + "@yahoo.com",
                Password = "marya",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "John",
                LastName = "Snow",
                Nickname = "Snowsnow",
                Email = "snow" + "@yahoo.com",
                Password = "snowsnow",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Test",
                LastName = "Test",
                Nickname = "Test",
                Email = "test" + "@yahoo.com",
                Password = "test",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Ghita",
                LastName = "Titulescu",
                Nickname = "Thedestroyer",
                Email = "ghitatit" + "@yahoo.com",
                Password = "thedestroyer",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "George",
                LastName = "Oltean",
                Nickname = "George",
                Email = "george" + "@yahoo.com",
                Password = "george",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Dan",
                LastName = "Negru",
                Nickname = "Dan344492",
                Email = "negerudan" + "@yahoo.com",
                Password = "dan344492",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Ion",
                LastName = "Ion",
                Nickname = "Ion98",
                Email = "ionelion" + "@yahoo.com",
                Password = "ion98",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Cata",
                LastName = "Catalin",
                Nickname = "Cata78",
                Email = "catacata" + "@yahoo.com",
                Password = "cata78",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Mara",
                LastName = "Pop",
                Nickname = "Mara11",
                Email = "popmara" + "@yahoo.com",
                Password = "mara11",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Laura",
                LastName = "Neamt",
                Nickname = "Laura77",
                Email = "lauralaura" + "@yahoo.com",
                Password = "laura77",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Ursu",
                LastName = "Ursu",
                Nickname = "Ursu00",
                Email = "ursuursu" + "@yahoo.com",
                Password = "ursu00",
                Status = "beginner",
                Experience = 0
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Ion22",
                LastName = "Ionescu",
                Nickname = "Ionelu22",
                Email = "ionion22" + "@yahoo.com",
                Password = "ionelu22",
                Status = "rocky",
                Experience = 1000
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
            player = new Player
            {
                FirstName = "Ion1233",
                LastName = "Ionescu132",
                Nickname = "Ionelu111",
                Email = "ionion111" + "@yahoo.com",
                Password = "ionelu111",
                Status = "rocky",
                Experience = 1000
            };
            players.InsertOne(player);
            findAllPlayers.Add(player);
        }

        public void AddGames()
        {
            IMongoCollection<Game> games = mongoDatabase.GetCollection<Game>("Games");
            Game game = new Game
            {
                Name = "Darts",
                Type = "X01"
            };
            games.InsertOne(game);
            findAllGames.Add(game);

            game = new Game
            {
                Name = "Darts",
                Type = "Cricket"
            };
            games.InsertOne(game);
            findAllGames.Add(game);
            game = new Game
            {
                Name = "Fussball",
                Type = "Sport"
            };
            games.InsertOne(game);
            findAllGames.Add(game);
            game = new Game
            {
                Name = "Monopoly",
                Type = "Table"
            };
            games.InsertOne(game);
            findAllGames.Add(game);
            game = new Game
            {
                Name = "Septica",
                Type = "Cards"
            };
            games.InsertOne(game);
            findAllGames.Add(game);
            game = new Game
            {
                Name = "Chess",
                Type = "Table"
            };
            games.InsertOne(game);
            findAllGames.Add(game);
            game = new Game
            {
                Name = "Fifa",
                Type = "Xbox"
            };
            games.InsertOne(game);
            findAllGames.Add(game);
        }

        public void AddMatches()
        {
            IMongoCollection<Match> matches = mongoDatabase.GetCollection<Match>("Matches");
            int playersCount = findAllPlayers.Count();
            Random rnd = new Random();

            foreach (Game game in findAllGames)
            {
                int count = 5;
                while (count > 0)
                {
                    int player1 = 0;
                    int player2 = 0;
                    while (player1 == player2)
                    {
                        player1 = rnd.Next(0, playersCount);
                        player2 = rnd.Next(0, playersCount);
                    }
                    Match match = new Match
                    {
                        IdGame = game.Id,
                        HomeTeam = new List<string> { findAllPlayers.ElementAt(player1).Nickname },
                        AwayTeam = new List<string> { findAllPlayers.ElementAt(player2).Nickname },
                        Winner = new List<string> { "nikname" },
                        Loser = new List<string> { "nikname" },
                        ScoreHomeTeam = new List<int> { rnd.Next(0,5) },
                        ScoreAwayTeam = new List<int> { rnd.Next(0, 5) },
                        dateTime = DateTime.Now.ToString(),
                        AddedByPlayer = new ObjectId().ToString()
                    };
                    matches.InsertOne(match);
                    count--;
                }
            }
        }

        public void AddChmapionships()
        {
            IMongoCollection<Championship> championships = mongoDatabase.GetCollection<Championship>("Championships");
            Championship championship = new Championship
            {
                Name = "Darts",
                Type = "X01",
                Participants = new List<string> { findAllPlayers.ElementAt(0).Nickname, findAllPlayers.ElementAt(1).Nickname, findAllPlayers.ElementAt(2).Nickname, findAllPlayers.ElementAt(3).Nickname, findAllPlayers.ElementAt(4).Nickname },
                CurentStatus = Championship.Status.Ongoing,
                FirstPlace = "",
                SecondPlace = "",
                ThirdPlace = ""
            };
            championships.InsertOne(championship);
            championship = new Championship
            {
                Name = "Fifa",
                Type = "Xbox",
                Participants = new List<string> { findAllPlayers.ElementAt(5).Nickname, findAllPlayers.ElementAt(2).Nickname, findAllPlayers.ElementAt(6).Nickname, findAllPlayers.ElementAt(8).Nickname, findAllPlayers.ElementAt(13).Nickname, findAllPlayers.ElementAt(7).Nickname },
                CurentStatus = Championship.Status.Ongoing,
                FirstPlace = "",
                SecondPlace = "",
                ThirdPlace = ""
            };
            championships.InsertOne(championship);

        }

        public void AddRules()
        {
            IMongoCollection<Rule> rules = mongoDatabase.GetCollection<Rule>("Rules");
            Rule rule = new Rule
            {
                Game = "Darts",
                Type = "X01",
                Description = "x01: x01 (pronouced oh-one)is probably the easiest game to learn. The rules are very simple, yet the game requires skill in order to play well and win. It is a very good game for beginners because it develops accuracy around the whole board and the simple rules allow the players to concentrate on their throwing.",
                AddedBy = "",
                DateAdded = "",
            };
            rules.InsertOne(rule);
            rule = new Rule
            {
                Game = "Darts",
                Type = "X01",
                Description = "Cricket is typically played between 2, 3 or 4 players, or teams of players, although the rules do not discount more players. The goal of cricket is to be the first player to open or close all the cricket numbers and have a higher or even point total",
                AddedBy = "",
                DateAdded = "",
            };
            rules.InsertOne(rule);
            rule = new Rule
            {
                Game = "Fifa",
                Type = "Xbox",
                Description = "Football (Soccer) is one of the oldest sports in the world and with that; it’s also one of the most recognised. The pinnacle of the international game comes in the form the Football World Cup. There are also tournament such as the Euro Championships, Copa America and the African Cup of Nations. Domestically the strongest leagues come from England (English Premier League), Spain (La Liga), Italy (Serie A) and Germany (Bundesliga). In parts of the world the sport is also known as Soccer.",
                AddedBy = "",
                DateAdded = "",
            };
            rules.InsertOne(rule);
            rule = new Rule
            {
                Game = "Monopoly",
                Type = "Table",
                Description = "To begin, place the game board on the table. Each player selects a token. Then they place their token on the table near the space labelled Go, placing it on Go only when his/her first turn to move arrives. One player becomes the Banker, who distributes assets from the Bank to the players. Only the player in question can use their money, money can only be lent via the Banker or by the player money. Each player is given $1,500 in cash divided as follows: two each of $500s, $100's and $50's; six $20's, five each of $10's, $5's and $1's. All remaining cash and other equipment go to the Bank. The Banker may play too but must keep his personal funds from the bank. He/she also needs to make sure that they collect $200 when they pass GO.",
                AddedBy = "",
                DateAdded = "",
            };
            rules.InsertOne(rule);
            rule = new Rule
            {
                Game = "Septica",
                Type = "Cards",
                Description = "Jocul si Obiectivele Septica este un joc care se joaca in 2 - 4 jucatori cu 32 de carti: 7,8,9,10,J,Q,K si A.artile  10 si A sunt puncte,iar cartile de 7 sunt taieri.De asemenea mai este taiere orice carte identica cu prima carte pusa pe masa.Exceptie face jocul in 3 unde se scot doua carti de 8,iar ceilalti doi de 8 ramasi in joc devin taieri.",
                AddedBy = "",
                DateAdded = "",
            };
            rules.InsertOne(rule);
        }
    }
}
