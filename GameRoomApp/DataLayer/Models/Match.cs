﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Models
{
    public class Match
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string IdGame { get; set; }

        public List<string> HomeTeam { get; set; }
        public List<string> AwayTeam { get; set; }
        public List<string> Winner { get; set; }
        public List<string> Loser { get; set; }
        public List<int> ScoreHomeTeam { get; set; }
        public List<int> ScoreAwayTeam { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string IdChampionship { get; set; }

        public string dateTime { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string AddedByPlayer { get; set; }
    }
}
