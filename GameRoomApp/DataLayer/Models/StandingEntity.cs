﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Models
{
    public class StandingEntity
    {
        public string Nickname { get; set; }
        public int Points { get; set; }
        public int MatchesPlayed { get; set; }
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Losses { get; set; }
        public int goalsFor { get; set; }
        public int goalsAgainst { get; set; }
    }
}
