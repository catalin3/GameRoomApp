﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Models
{
    public class Rule
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Game { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string AddedBy { get; set; }
        public string DateAdded { get; set; }
    }
}
