﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameRoomApp.DataLayer.Models
{
    public class Championship
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { set; get; }
        public string Name { get; set; }
        public string Type { get; set; }
        public List<string> Participants { get; set; }
        public enum Status { Ongoing, End };
        public Status CurentStatus { set; get; }
        public string FirstPlace { get; set; }
        public string SecondPlace { get; set; }
        public string ThirdPlace { get; set; }

    }
}
