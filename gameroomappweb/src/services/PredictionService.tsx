import axios, { AxiosResponse } from 'axios';
import { config } from 'src/Helpers/config';

export default class PredictionService{
    
    public static getCategory = (image: any) => {
        //var image: string = stringify(data);
        return axios
            .post(`${config.apiUrlPrediction}`,image , config.restDbconfig)
            .then((result: AxiosResponse) => result.data);
    }
}