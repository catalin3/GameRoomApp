import Axios, { AxiosResponse } from 'axios';
import { config } from 'src/Helpers/config';
import { IChampionship } from 'src/models/IChampionship';


export default class ChampionshipService {

    public static getChampionshipsByGame = (name: string, type: string) => {
        return Axios
            .get(`${config.apiUrl}/championship?Name=${name}&Type=${type}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static addChampionship = (championship: IChampionship): Promise<IChampionship> => {
        return Axios
        .post(`${config.apiUrl}/championship`, championship)
        .then((result: AxiosResponse) => result.data)
        .catch(errors => console.log(errors));
    } 

    public static getChampionshipById = (id: string) => {
        return Axios
            .get(`${config.apiUrl}/championship?Id=${id}`)
            .then((result: AxiosResponse) => result.data)
    }

    public static getStatus = (status: string) => {
        if(status == "0"){
            return "Ongoing";
        }
        return "End";
    }
    
}