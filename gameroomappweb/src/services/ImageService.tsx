import axios, { AxiosResponse } from 'axios';
import { config } from 'src/Helpers/config';
import { IImageMoment } from 'src/models/IImageModel';

export default class ImageService {

    public static addImage = (image: IImageMoment): Promise<IImageMoment> => {
        return axios
                .post(`${config.apiUrl}/image`, image)
                .then((result: AxiosResponse) => result.data)
                .catch(errors => console.log(errors));
    }

    public static existGame(games: string[], game: string): boolean{
        for(var i=0;i<games.length;i++){
            if(game[i].toUpperCase() == game.toUpperCase()){
                return true;
            }
        }
        return false;
    }

    public static getImages = () => {
        return axios
                .get(`${config.apiUrl}/image`)
                .then((result: AxiosResponse) => result.data);
    }
    public static getImagesByGame = (game: string) => {
        return axios   
            .get(`${config.apiUrl}/image?Game=${game}`)
            .then((result: AxiosResponse) => result.data);
    } 

    public static getGamesName = () => {
        return axios
                .get(`${config.apiUrl}/image?OnlyGames=${"true"}`)
                .then((result: AxiosResponse) => result.data);
    } 

    public static deleteImage = (imageId: string) => {
        return axios 
                .delete(`${config.apiUrl}/image/${imageId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getLast10 = () => {
        return axios    
        .get(`${config.apiUrl}/image?Last=${"true"}`)
        .then((result: AxiosResponse) => result.data);
    }

}