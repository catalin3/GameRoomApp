import axios, { AxiosResponse } from 'axios';
import { IGame } from 'src/models/IGame';
import { config } from 'src/Helpers/config';

export default class GameService{

    public static getAllGames = (): Promise<IGame[]> => {
        return axios
                .get(`${config.apiUrl}/game`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getGameByNameAndType = (name: string, type: string): Promise<IGame> => {
        return axios
                .get(`${config.apiUrl}/game?Name=${name}&Type=${type}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getGameByName = (name: string): Promise<IGame[]> => {
        return axios
                .get(`${config.apiUrl}/game?Name=${name}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getGameById = (idGame: string): Promise<IGame> => {
        return axios
                .get(`${config.apiUrl}/game/Id=${idGame}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertGame = (game: IGame): Promise<IGame> => {
        return axios
                .post(`${config.apiUrl}/game?Name=${game.name}&Type=${game.type}`)
                .then((result: AxiosResponse) => result.data);
    }
}
