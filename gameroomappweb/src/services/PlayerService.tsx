import axios, { AxiosResponse } from 'axios';
import { config } from 'src/Helpers/config';

export default class PlayerService {
    public static addPlayer = (player: IPlayer): Promise<IPlayer> => {
        return axios
                .post(`${config.apiUrl}/player`, player)
                .then((result: AxiosResponse) => result.data)
                .catch(errors => console.log(errors));
    }

    public static getAllPlayers = (): Promise<IPlayer[]> => {
        return axios
                .get(`${config.apiUrl}/player`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getPlayersPage = (pageNumber: number, pageSize: number, property: string, sort: string): Promise<IPlayer[]> => {
        return axios
            .get(`${config.apiUrl}/player?PageNumber=${pageNumber}&PageSize=${pageSize}&Property=${property}&sort=${sort}`)
            .then((result: AxiosResponse) => result.data);
    }

    public static getNumberOfPlayers = () => {
        return axios
            .get(`${config.apiUrl}/player?Count=${"true"}`)
            .then((result: AxiosResponse) => result.data);
    }

    public static existPlayer(players: IPlayer[], nickname: string): boolean{
        for(var i=0;i<players.length;i++){
            if(players[i].nickname == nickname){
                return true;
            }
        }
        return false;
    }

    public static getChampionshipStandings = (idChampionship: string) => {
        return axios  
            .get(`${config.apiUrl}/player?IdChampionship=${idChampionship}&Table=${"true"}`)
            .then((result: AxiosResponse) => result.data);
    }
}