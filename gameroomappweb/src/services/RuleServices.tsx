import axios, { AxiosResponse } from 'axios';
import { IRule } from 'src/models/IRule';
import { config } from 'src/Helpers/config';

export default class RuleService{

    public static getAllRules = (): Promise<IRule[]> => {
        return axios
                .get(`${config.apiUrl}/rule`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getRuleByGameAndType = (game: string, type: string): Promise<IRule> => {
        return axios
                .get(`${config.apiUrl}/rule?Game=${game}&Type=${type}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getRuleByGame = (game: string): Promise<IRule[]> => {
        return axios
                .get(`${config.apiUrl}/rule?Game=${game}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertRule = (rule: IRule): Promise<IRule> => {
        return axios
                .post(`${config.apiUrl}/rule`,rule)
                .then((result: AxiosResponse) => result.data);
    }

    public static deleteRule = (ruleId: string): Promise<any> => {
        return axios
                .delete(`${config.apiUrl}/rule/${ruleId}`)
                .then((result: AxiosResponse) => result.data);
    }
}
