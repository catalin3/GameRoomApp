import { config } from 'src/Helpers/config';
import axios, { AxiosResponse } from 'axios';
import * as decode from 'jwt-decode';

export default class UserService{

    public static login2 = (user: IPlayer): Promise<any> => {
        return axios
                .post(`${config.apiUrl}/user`, user)
                .then((result: AxiosResponse) => result.data);
    }

    public static login = (nickname: string, password: string): Promise<any> => {
        var user: IPlayer = {
            nickname: nickname,
            password: password
        }
        return axios
                .post(`${config.apiUrl}/user`, user)
                .then((result: AxiosResponse) => result.data);
        }

    public static setToken = (token: any) => {
        window.localStorage.setItem("token", token);
        console.log("Saved token "+window.localStorage.getItem("token") + "in storage");
    }

    public static getToken(){
        return window.localStorage.getItem("token");
    }

    public static loggedIn(){
        const token: any = this.getToken();
        return !!token && !this.isTokenExpired(token);
    }

    public static isTokenExpired = (token: string) => {
        try{
            const decoded: any = decode(token)
            if(decoded.isTokenExpired){
                return true;
            }
            else
                return false;
        }
        catch(err){
            return false;
        }
        
    }

    public static logout(){
        window.localStorage.removeItem("token"); 
    }

    public static getProfile(){
        return decode(this.getToken()||'');
    }
}