import axios, { AxiosResponse } from 'axios';
import { config } from 'src/Helpers/config';
import { IMatch } from 'src/models/IMatch';
//import * as mongoose from 'mongoose';

export default class MatchService {

    public static getMatchesByGame = (name: string, type: string) => {
        return axios
                .get(`${config.apiUrl}/match?Name=${name}&Type=${type}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getMatchesByChampionship = (id: string) => {
        return axios
                .get(`${config.apiUrl}/match?IdChampionship=${id}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getMatchById = (id: string) => {
        return axios
                .get(`${config.apiUrl}/match?Id=${id}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static addMatch = (match: IMatch): Promise<IMatch> => {
        return axios
                .post(`${config.apiUrl}/match`, match)
                .then((result: AxiosResponse) => result.data)
                .catch(errors => console.log(errors));
    }

    public static deleteMatch = (matchId: string): Promise<any> => {
        return axios
                .delete(`${config.apiUrl}/match/${matchId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static updateMatch = (matchId: string, match: IMatch): Promise<any> => {
        return axios
                .put(`${config.apiUrl}/match/${matchId}`, match)
                .then((result: AxiosResponse) => result.data);
    }

    public static getLast10 = () => {
        return axios    
        .get(`${config.apiUrl}/match?Last=${"true"}`)
        .then((result: AxiosResponse) => result.data);
    }

}