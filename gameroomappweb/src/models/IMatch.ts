
export interface IMatch{
    id: string;
    idGame: string;
    homeTeam: Array<string>;
    awayTeam: Array<string>;
    winner?: Array<string>;
    loser?: Array<string>;
    scoreHomeTeam: Array<number>;
    scoreAwayTeam: Array<number>;
    idChampionship?: string;
    dateTime?: string;
    addedByPlayer?: string;
}