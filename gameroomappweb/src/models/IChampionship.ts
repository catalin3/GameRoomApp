
export interface IChampionship{
    id:string;
    name:string;
    type:string;
    participants:string[];
    curentStatus:string;
    firstPlace:string;
    secondPlace:string;
    thirdPlace:string;
}