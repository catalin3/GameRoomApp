interface IPlayer{
    _id?: string
    nickname: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    status?: string;
    experience?: number;
    password: string,
    token?: string
}