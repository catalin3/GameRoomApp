export interface IImageMoment{
    id: string
    name: string;
    imageDataAsString: string;
    game?: string;
    description?: string;
}