
export interface IStandingEntity{
    nickname: string;
    matchesPlayed: number;
    points: number;
    wins: number;
    losses: number;
    draws: number;
    goalsFor: number;
    goalsAgainst: number;
}