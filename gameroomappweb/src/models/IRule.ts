
export interface IRule{
    id:string;
    game:string;
    type:string;
    description:string;
    addedBy?:string;
    dataAdded?:string; 
}