export const config = {
    apiUrl:"https://localhost:44357/api",
    apiUrlPrediction:"https://southcentralus.api.cognitive.microsoft.com/customvision/v2.0/Prediction/dcef5859-1fb2-4091-b0b9-018cbb0f5744/image?iterationId=72df5781-94b8-4f8a-9f32-17682a652392",
    restDbconfig: {
        headers: {
            "Prediction-Key": "08c5182517c54e99bd3479116be78c30",
            "Content-Type": "application/octet-stream"
        }
    }
};