import * as React from 'react';
import './_game.scss';
import { IGame } from '../../../models/IGame';

export interface IGameProps {
    item:IGame;
    selectGame(item: IGame): void;
}

const Game  = (props: IGameProps) => {
    return(
        <div key={props.item.id} className="gr-main-game">
            <span className="gr-game" onClick={() => {props.selectGame(props.item)}}>{`${props.item.name} - ${props.item.type}`}</span>
        </div>
    );
}

export default Game;