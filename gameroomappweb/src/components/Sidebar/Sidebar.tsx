import * as React from 'react';
import { IGame } from 'src/models/IGame';
import GameService from 'src/services/GameService';
import Game from './Game/game';
import { Link } from 'react-router-dom';
import './Sidebar.scss';

export interface ISidebarProps {
    onGameSelected(id: string, name: string, type:string): void;
}

export interface ISidebarState {
    games: IGame[];
    loading: boolean;
}

export default class Sidebar extends React.Component<ISidebarProps,ISidebarState> {
    
    constructor(props: any){
        super(props);
        this.state = {
            games: [],
            loading: true
        }
    }

    public componentDidMount() {
        GameService.getAllGames().then((games: IGame[]) => {
            this.setState({
                games: games,
                loading: false
            });
        });
    }

    public selectGame = (game: IGame) => {
        if(!game.id && !game.name && !game.type) return;
        this.props.onGameSelected(game.id, game.name, game.type);
    }

    public render() {
        return(
            <div className="gr-main-sidebar">
                <div className="gr-div-button addGame">
                    <Link className="gr-button-addGame" to='/game'>Add Game</Link>
                </div>
                <div className="gr-sidebar">
                    <h1>Games</h1>
                    {this.state.loading && <h1>Loading</h1>}

                    {!this.state.loading && 
                        this.state.games.map((item,index) => (
                                <Game 
                                    key={index}
                                    item={item}
                                    selectGame={this.selectGame}
                                ></Game>
                        
                        )
                        )}
                </div>
               
            </div>
        );
    }
}