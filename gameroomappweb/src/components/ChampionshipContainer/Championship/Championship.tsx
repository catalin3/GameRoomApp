import * as React from 'react';
import './_championship.scss';
import { IChampionship } from '../../../models/IChampionship'
import { Link } from 'react-router-dom';
import ChampionshipService from 'src/services/ChampionshipService';

export interface IChampionshipProps {
    item: IChampionship;
    //selectChampionship(championship: IChampionship): void;
    //deleteMatch(item: string): void;
}

const Championship = (props: IChampionshipProps) => {
    return(
        <div key={props.item.id} className="gr-championship-entitie">
            <div className="gr-championship-details">
                <span /*onClick={() => {props.selectChampionship(props.item)}}*/>
                    <strong>{`${props.item.name}`}</strong>    <p className="score">{`${props.item.type} - ${ChampionshipService.getStatus(props.item.curentStatus)}`}</p>     <strong>{`[${props.item.participants}]`}</strong>
                </span>
                <div className="gr-championship-data">
                    <Link to={`/championshipresult/${props.item.id}`}>FULL DETAILS</Link>  
                </div>
            </div>
        </div>
    );
}

export default Championship;