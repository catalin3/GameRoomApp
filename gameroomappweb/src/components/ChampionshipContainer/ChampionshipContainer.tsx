import * as React from 'react';
import './_championshipContainer.scss';
import ChampionshipService from 'src/services/ChampionshipService';
import { IChampionship } from 'src/models/IChampionship';
import Championship from './Championship/Championship';
import { Link } from 'react-router-dom';

export interface IChampionshipContainerProps{
    idGame: string;
    name: string;
    type: string;
    //onChampionshipSelected(idChampionship: string):void
}

export interface IChampionshipContainerState{
    championships: Array<IChampionship>;
    loading: boolean;
}

export default class ChampionshipContainer extends React.Component<IChampionshipContainerProps,IChampionshipContainerState>{
    constructor(props: IChampionshipContainerProps){
        super(props);

        this.state = {
            championships: [],
            loading: false
        }
    }

    componentWillReceiveProps(props: IChampionshipContainerProps){
        this.setState({
            loading: true
        });

        ChampionshipService.getChampionshipsByGame(props.name, props.type)
            .then((result: Array<IChampionship>) => {
                this.setState({
                    championships: result,
                    loading: false
                });
            });

        
    }

    selectChampionship = (championship: IChampionship) => {
        if(!championship.id && !championship.name && !championship.type) return;
        //this.props.onChampionshipSelected(championship.id);
    }

    public render() {
        return (
            this.props.idGame !== '' &&
            <div className="gr-championshipcontainer">
                <div className="gr-div-button">
                    <Link className="gr-button-addChampionship" to={`/championship/${this.props.name}/${this.props.type}`}>Add Championship</Link>
                </div>
                <h1>Championships</h1>
                {this.state.loading && <h1>Loading</h1>}

                {!this.state.loading &&
                    this.state.championships.map((item, index) => (
                        <div key={index}>
                            <Championship 
                                item={item}
                                //selectChampionship={this.selectChampionship}
                            ></Championship>
                        </div>
                    )
                    )}
            </div>
        );
    }
}