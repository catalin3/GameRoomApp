import * as React from 'react';
import './_gameSidebarManager.scss';
import { RouteComponentProps } from 'react-router';
import GameService from 'src/services/GameService';
import { IGame } from 'src/models/IGame';
import * as mongoose from 'mongoose';

export interface IGameSidebarManagerState {
    errorMessage: string;
    name: string;
    type: string;
    games: IGame[];
}

export interface IGameSidebarProps extends RouteComponentProps<any>{}

export default class GameSidebarManager extends React.Component<IGameSidebarProps, IGameSidebarManagerState> {
    
    constructor(props: IGameSidebarProps){
        super(props);

        this.state = {
            errorMessage: '',
            name: '',
            type: '',
            games: []
        }
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;   
        this.setState((prevState: IGameSidebarManagerState) => (
        {
            ...prevState,
            [name]: value
        }));
    }

    componentDidMount(){
        GameService.getAllGames().then((games: IGame[]) => {
            this.setState({
                games: games
            });
        });
        if(this.props.match.params.id != undefined){
            GameService.getGameByNameAndType(this.props.match.params.name, this.props.match.params.name,)
                .then((game: IGame) => {
                    this.setState({
                        name: game.name,
                        type: game.type
                    });
                });
        }
    }

    addGame = () => {
        if(this.validate()){
            const newGame:IGame = {
                id: String(mongoose.Types.ObjectId()),
                name: this.state.name,
                type: this.state.type
            }
            GameService.insertGame(newGame)
                .then(() => {
                    this.props.history.push('/matches');
                });
        }
    }


    validate = () => {
        if(this.state.name == ''){
            this.setState({errorMessage: `Add a Name!`});
            return false;
        }
        if(this.state.type == ''){
            this.setState({errorMessage: `Add a Type!`});
            return false;
        }
        for(var i =0; i < this.state.games.length; i++){
            if(this.state.name == this.state.games[i].name && this.state.type == this.state.games[i].type){
                this.setState({errorMessage: `This game already exist!`});
                    return false;
            }
        }
        return true;
    }

    public render() {
        return (
            <div className="App">
                <div className="gr-gameform">
                    <h1>Add new Game</h1>
                    <input 
                        type="text"
                        name="name" 
                        onChange={this.handleChange} 
                        value={this.state.name}
                        placeholder={"Name"}
                    />
                    <br/>
                    <input 
                        type="text" 
                        name="type" 
                        onChange={this.handleChange} 
                        value={this.state.type}
                        placeholder={"Type"}
                    />
                    <div>
                        {
                        this.props.match.params.id === undefined ?
                            <button className="gr-addGame" onClick={this.addGame}>Add Game</button>:
                            <button></button>
                        }

{
                        this.state.errorMessage ?
                        <div className="gr-matchErrorMessage">
                            {this.state.errorMessage}
                        </div>:
                        ''
                    }
                    </div>
                </div>
                
            </div>
        )
    }
}