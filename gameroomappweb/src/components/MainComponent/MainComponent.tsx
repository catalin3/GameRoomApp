import * as React from 'react';
import Header from '../Header/Header';
import Root from 'src/Root';

export interface IMainComponentProps{
    handleSetToken: (token: string)=> void;
}

export default class MainComponent extends React.Component<IMainComponentProps,any>{
    constructor(props: IMainComponentProps){
        super(props);
    }



    public render(){
        return (
            <div className = "gr-app-main">
                <Header handleSetToken={this.props.handleSetToken}/>
                <Root/>
            </div>
        )
    }
}
