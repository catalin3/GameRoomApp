import * as React from 'react';
import './_gameContainer.scss';
import { IMatch } from 'src/models/IMatch';
import MatchService from 'src/services/MatchService';
import Match from './Match/Match';
import { Link } from 'react-router-dom';


export interface IGameContainerProps {
    idGame: string;
    name: string;
    type: string;
}

export interface IGameContainerState {
    matches: Array<IMatch>;
    loading: boolean;
}

export default class GameContainer extends React.Component<IGameContainerProps, IGameContainerState>{

    constructor(props: IGameContainerProps) {
        super(props);

        this.state = {     
            matches: [],
            loading: false
        }
    }

    componentWillReceiveProps(props: IGameContainerProps) {
        this.setState({
            loading: true
        });
        MatchService.getMatchesByGame(props.name, props.type)
            .then((result: Array<IMatch>) => {
                this.setState({
                    matches: result,
                    loading: false
                });
            });
    }

    public deleteMatch = (matchId: string) => {
        MatchService.deleteMatch(matchId)
            .then(() => {
                this.setState((previousState: IGameContainerState) => ({
                    matches: [...previousState.matches.filter(m => m.id != matchId)]
                }));
            })
    }

    public render() {
        return (
            this.props.idGame !== '' &&
            <div className="gr-gamecontainer">
                <div className="gr-div-button">
                    <Link className="gr-button-addMatch" to={`/match/${null}/${this.props.idGame}/${null}`}>Add Match</Link>
                </div>
                <h1>Matches</h1>
                {this.state.loading && <h1>Loading</h1>}
                
                {!this.state.loading &&
                    this.state.matches.map((item, index) => (
                        <div className="match" key={index}>
                            <Match
                                item={item}
                                // selectMatch={this.selectMatch}
                                deleteMatch={this.deleteMatch}
                            ></Match>
                        </div>
                    )
                    )}
            </div>
        );
    }
}