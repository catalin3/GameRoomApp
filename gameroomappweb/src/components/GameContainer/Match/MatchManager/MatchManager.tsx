import * as React from 'react';
import './_matchManager.scss';
import { RouteComponentProps } from 'react-router';
import { IMatch } from 'src/models/IMatch';
import MatchService from 'src/services/MatchService';
import * as mongoose from 'mongoose';
import PlayerService from 'src/services/PlayerService';

export interface IMatchManagerState{
    errorMessage: string;
    homeTeam: string;
    awayTeam: string;
    scoreHomeTeam: string;
    scoreAwayTeam: string;
    players: IPlayer[];
}

export interface IMatchManagerProps extends RouteComponentProps<any>{}

export default class MatchManager extends React.Component<IMatchManagerProps,IMatchManagerState>{
    constructor(props: IMatchManagerProps){
        super(props);

        this.state = {
            errorMessage: '',
            homeTeam: '',
            awayTeam: '',
            scoreHomeTeam: '0',
            scoreAwayTeam: '0',
            players: []
        }
    }

    componentDidMount() {
        PlayerService.getAllPlayers().then((players: IPlayer[]) => {
            this.setState({
                players: players
            });
        });
        if(this.props.match.params.id != "null") {
            MatchService.getMatchById(this.props.match.params.id)
                .then((match: IMatch) => {
                    this.setState({
                        homeTeam : String(match.homeTeam).replace('[','').replace(']',''),
                        awayTeam : String(match.awayTeam).replace('[','').replace(']',''),
                        scoreHomeTeam: String(match.scoreHomeTeam),
                        scoreAwayTeam: String(match.scoreAwayTeam)
                    })
                })
        }
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IMatchManagerState) => (
        {
                ...prevState,
                [name]: value
            
        }));
    }

    addMatch = () => {
        if(this.validate()){
            let newMatch = this.buildMatch();
            newMatch.idGame = this.props.match.params.idGame;
            MatchService.addMatch(newMatch)
                .then(() => {
                    this.props.history.push('/matches');
                });
        }
    }


    updateMatch = () => {
        if(this.validate()){
            let updatedMatch = this.buildMatch();
            updatedMatch.id = this.props.match.params.id;
            updatedMatch.idGame = this.props.match.params.idGame;
            updatedMatch.idChampionship = this.props.match.params.idChampionship;
            MatchService.updateMatch(this.props.match.params.id, updatedMatch)
                .then(() => {
                    this.props.history.goBack();
                });
        }
    }

    

    buildMatch() {
        
        let match: IMatch = {
            id: String(mongoose.Types.ObjectId()),
            idGame: this.props.match.params.idGame,
            homeTeam: this.state.homeTeam.split(','),
            awayTeam: this.state.awayTeam.split(','),
            scoreHomeTeam: [parseInt(this.state.scoreHomeTeam)],
            scoreAwayTeam: [parseInt(this.state.scoreAwayTeam)],
            winner: this.winnerTeam(),
            loser: this.loserTeam(),
        }
         return match;
    }
    
    winnerTeam = () => {
        if(this.state.scoreHomeTeam > this.state.scoreAwayTeam){
            return this.state.homeTeam.split(',');
        }
        return [""];
    }

    loserTeam = () => {
        if(this.state.scoreHomeTeam > this.state.scoreAwayTeam){
            return this.state.awayTeam.split(',');
        }
        return [""];
    }

    validateTeamMember = (teamMembers: string) => {
        if(teamMembers==''){
            this.setState({errorMessage: `Add a player` })
                return false;
        }
        var splitTeamMembers = teamMembers.split(',');
        if(splitTeamMembers.length == 0){
            this.setState({errorMessage: `Player does not exist!` })
                return false;
        }
        for(var i =0; i < teamMembers.length; i++){
            if(splitTeamMembers[i] != undefined)
                if(!PlayerService.existPlayer(this.state.players, splitTeamMembers[i])){
                    this.setState({errorMessage: `Player ${splitTeamMembers[i]} does not exist!` })
                    return false;
                }
        }
        return true;
    }

    validate = () => {
        if(!this.validateTeamMember(this.state.homeTeam)){
            return false;
        }
        if(!this.validateTeamMember(this.state.awayTeam)){
            return false;
        }
        this.setState({errorMessage:''});
        return true;
    }

    public render() {
        return (
            <div>
                <div className="gr-matchform">
                        <h1>Match</h1>
                        
                        <input placeholder={"Home Team"} type="text" name="homeTeam" onChange={this.handleChange} value={this.state.homeTeam}/><br/>
                        <input type="number" name="scoreHomeTeam" onChange={this.handleChange} value={this.state.scoreHomeTeam.toString()}/><br/>
                        <br/>

                        <input placeholder={"Away Team"} type="text" name="awayTeam" onChange={this.handleChange} value={this.state.awayTeam}/><br/>
                        <input type="number" name="scoreAwayTeam" onChange={this.handleChange} value={this.state.scoreAwayTeam.toString()}/><br/>
                    
                    <div>
                    {
                        this.props.match.params.id == "null"?
                            <button className="gr-addMatch" onClick={this.addMatch}>Add</button>:
                            <button className="gr-updateMatch" onClick={this.updateMatch}>Update</button>
                    }
                    
                    {
                        this.state.errorMessage ?
                        <div className="gr-matchErrorMessage">
                            {this.state.errorMessage}
                        </div>:
                        ''
                    }
                    </div>                
                </div>
            </div>
        )
    }
}
