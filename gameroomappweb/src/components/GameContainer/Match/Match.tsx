import * as React from 'react';
import { IMatch } from 'src/models/IMatch';
import './_match.scss';
import { Link } from 'react-router-dom';

export interface IMatchProps {
    item: IMatch;
    // selectMatch(match: IMatch): void;
    deleteMatch(item: string): void;
}

const Match = (props: IMatchProps) => {
    return(
        
            <div className="gr-match">
                <div className="gr-match-details">
                    {
                        props.item.scoreHomeTeam == null?
                        <div>
                            <strong>{`[${props.item.homeTeam}]`}</strong>    
                            <p className="score"> Not played </p>     
                            <strong>{`[${props.item.awayTeam}]`}</strong>
                        </div>:
                        <div>
                            <strong>{`[${props.item.homeTeam}]`}</strong>    
                            <p className="score">{`${props.item.scoreHomeTeam} - ${props.item.scoreAwayTeam}`}</p>     
                            <strong>{`[${props.item.awayTeam}]`}</strong>
                        </div>
                    }
                    
                </div>
                    <br/>
                    <button><Link to={`/match/${props.item.id}/${props.item.idGame}/${props.item.idChampionship}`}>Update</Link></button>
                    <button onClick={() => props.deleteMatch(props.item.id)}>Delete</button>
                    <br/><br/>
                
                
            </div>
        
    );
}

export default Match;