import * as React from 'react';

export interface IPlayerProps {
    item: IPlayer;
}

const Player = (props: IPlayerProps) => {
    return (
        <tr key={props.item._id}>
            <td>{`${props.item.nickname}`}</td>
            <td>{`${props.item.firstName}`}</td>
            <td>{`${props.item.lastName}`}</td>
            <td>{`${props.item.status}`}</td>
            <td>{`${props.item.experience}`}</td>
            <td>{`${props.item.email}`}</td>
        </tr>
    )
}

export default Player;
