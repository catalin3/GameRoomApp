import * as React from 'react';
import './_standings.scss';
import PlayerService from 'src/services/PlayerService';
import Player from './Player/Player';
import { Pagination } from 'react-bootstrap';

export interface IPlayerState {
    players: IPlayer[];
    loading: boolean;

    sizeOfPage: number;
    firstPage: number;
    lastPage: number;
    curentPage: number;
    nextPage: number;
    prevPage: number;

    property: string;
    sort: string;
}

export default class Standings extends React.Component<any, IPlayerState>{

    constructor(props: any){
        super(props)
        this.state = {
            players: [],
            loading: true,
            sizeOfPage: 20,
            firstPage: 0,
            lastPage: 0,
            curentPage: 0,
            nextPage: 0,
            prevPage: 0,
            property: "Experience",
            sort: "Descending"
        }
    }

    componentDidMount() {
        this.setState({
            sizeOfPage:  20
        });
        PlayerService.getNumberOfPlayers()
            .then((response: number) =>{
                console.log("number of elements: "+response);
                this.setState({
                    lastPage: (~~(response/this.state.sizeOfPage))
                });
                console.log("Pages: "+this.state.lastPage);
            })
        PlayerService.getPlayersPage(0,this.state.sizeOfPage,this.state.property, this.state.sort).then((players: IPlayer[]) => {
            this.setState({
                players: players,
                loading: false,
                firstPage: 0,
                nextPage: 1,
                prevPage: -1,
                curentPage: 0
            });
        });
    }

    changePage =(firstPage: number, prevPage: number, curentPage: number, nextPage: number, lastPage: number, sizeOfPage: number, property: string, sort: string) => {
        if(curentPage >= firstPage && curentPage <= lastPage){
            PlayerService.getPlayersPage(curentPage, sizeOfPage, property, sort).then((players: IPlayer[]) => {
                this.setState({
                    players: players,
                    loading: false,
                    firstPage: firstPage,
                    prevPage: prevPage,
                    curentPage: curentPage,
                    nextPage: nextPage,
                    lastPage: lastPage
                })
            })
        }
    }

    changePropertySort = (firstPage: number, prevPage: number, curentPage: number, nextPage: number, lastPage: number, sizeOfPage: number, property: string, sort: string) => {
        this.setState({
            property: property,
            sort: sort
        })
        this.changePage(firstPage,firstPage-1,firstPage,firstPage+1,lastPage,sizeOfPage,property,sort);
    }

    public render() {

        return (
            <div className="gr-standings-main">
                <h1>Players</h1>

                <Pagination className="gr-standings-pagination-main">
                    <Pagination.First   className = "gr-pagination-elem first"
                                        onClick={() => this.changePage(
                                            this.state.firstPage,
                                            this.state.firstPage-1,
                                            this.state.firstPage,
                                            this.state.firstPage+1,   
                                            this.state.lastPage,
                                            this.state.sizeOfPage,
                                            this.state.property,
                                            this.state.sort
                                        )}
                    />
                    <Pagination.Prev    className = "gr-pagination-elem prev"
                                        onClick={() => this.changePage(
                                            this.state.firstPage,
                                            this.state.prevPage-1,
                                            this.state.prevPage,
                                            this.state.curentPage,   
                                            this.state.lastPage,
                                            this.state.sizeOfPage,
                                            this.state.property,
                                            this.state.sort
                                        )}
                    />
                    <Pagination.Item    className = "gr-pagination-elem"
                                        onClick={() => this.changePage(
                                            this.state.firstPage,
                                            this.state.firstPage-1,
                                            this.state.firstPage,
                                            this.state.firstPage+1,   
                                            this.state.lastPage,
                                            this.state.sizeOfPage,
                                            this.state.property,
                                            this.state.sort
                                        )}
                    >{this.state.firstPage}</Pagination.Item>
                    <Pagination.Ellipsis  className = "gr-pagination-elem ellipsis"/>
                    
                    {
                        (~~(this.state.lastPage/2)) > 2?
                        <Pagination.Item    className = "gr-pagination-elem"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                ~~(this.state.lastPage/2)-1,
                                                ~~(this.state.lastPage/2),
                                                ~~(this.state.lastPage/2)+1,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}
                        >{~~(this.state.lastPage/2)}</Pagination.Item>:
                        ''
                    
                    }

                    <Pagination.Ellipsis  className = "gr-pagination-elem ellipsis"/>
                    <Pagination.Item    className = "gr-pagination-elem"
                                        onClick={() => this.changePage(
                                            this.state.firstPage,
                                            this.state.lastPage-1,
                                            this.state.lastPage,
                                            this.state.lastPage+1,   
                                            this.state.lastPage,
                                            this.state.sizeOfPage,
                                            this.state.property,
                                            this.state.sort
                                        )}
                    >{this.state.lastPage}</Pagination.Item>
                    <Pagination.Next    className = "gr-pagination-elem next"
                                        onClick={() => this.changePage(
                                            this.state.firstPage,
                                            this.state.curentPage,
                                            this.state.curentPage+1,
                                            this.state.nextPage+1,   
                                            this.state.lastPage,
                                            this.state.sizeOfPage,
                                            this.state.property,
                                            this.state.sort
                                        )}  
                        />
                    <Pagination.Last    className = "gr-pagination-elem last"
                                        onClick={() => this.changePage(
                                            this.state.firstPage,
                                            this.state.lastPage-1,
                                            this.state.lastPage,
                                            this.state.lastPage+1,   
                                            this.state.lastPage,
                                            this.state.sizeOfPage,
                                            this.state.property,
                                            this.state.sort
                                        )}
                    />
                </Pagination>

                {this.state.loading && <h1>Loading</h1>}
                <table className="gr-table-standings">
                    <thead className="gr-head-standings">
                        <tr>
                            <th>Nickname<button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Nickname",
                                                    "Ascending"     
                            )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Nickname",
                                                    "Descending"     
                            )}>{">"}</button></th>
                            <th>First Name<button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "FirstName",
                                                    "Ascending"     
                            )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "FirstName",
                                                    "Descending"     
                            )}>{">"}</button></th>
                            <th>Last Name<button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "LastName",
                                                    "Ascending"     
                            )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "LasttName",
                                                    "Descending"     
                            )}>{">"}</button></th>
                            <th>Status<button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Status",
                                                    "Ascending"     
                            )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Status",
                                                    "Descending"     
                            )}>{">"}</button></th>
                            <th>Experience<button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Experience",
                                                    "Ascending"     
                            )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Experience",
                                                    "Descending"     
                            )}>{">"}</button></th>
                            <th>Email<button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Email",
                                                    "Ascending"     
                            )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                    this.state.firstPage,
                                                    this.state.firstPage-1,
                                                    this.state.firstPage,
                                                    this.state.firstPage+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    "Email",
                                                    "Descending"     
                            )}>{">"}</button></th>
                        </tr>
                    </thead>
                    <tbody>
                    

                    {!this.state.loading &&
                        this.state.players.map((item, index) => (
                            <Player
                                key={index}
                                item={item}
                            >
                            </Player>
                        )
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}