import * as React from 'react';
import './_championshipManager.scss';
import { RouteComponentProps } from 'react-router';
import { IChampionship } from 'src/models/IChampionship';
import * as  mongoose  from 'mongoose';
import ChampionshipService from 'src/services/ChampionshipService';
import PlayerService from 'src/services/PlayerService';

export interface IChampionshipManagerState{
    participants: string;
    errorMessage: string;
    players: IPlayer[];
}

export interface IChampionshipManagerProps extends RouteComponentProps<any> {

}


export default class ChampionshipManager extends React.Component<IChampionshipManagerProps, IChampionshipManagerState> {
    constructor(props: IChampionshipManagerProps){
        super(props);

        this.state = {
            participants: '',
            errorMessage: '',
            players: []
        }
    }

    public componentDidMount() {
        PlayerService.getAllPlayers().then((players: IPlayer[]) => {
            this.setState({
                players: players
            });
        });
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IChampionshipManagerState) => (
            {
                ...prevState,
                [name]: value
            }
        ));
    }

    addChampionship = () => {
        if(this.validate()){
            let newChampionship: IChampionship = {
                id: String(mongoose.Types.ObjectId()),
                name: this.props.match.params.name,
                type: this.props.match.params.type,
                participants: this.state.participants.split(','),
                curentStatus: '0',
                firstPlace: '',
                secondPlace: '',
                thirdPlace: ''
            }
            ChampionshipService.addChampionship(newChampionship)
                .then(() => {
                    this.props.history.push('/championships');
                });
        }
    }

    validateParticipants = (participants: string) => {
        if(participants==''){
            this.setState({errorMessage: `Add a player` })
                return false;
        }
        var splitParticipants = participants.split(',');
        if(splitParticipants.length == 0){
            this.setState({errorMessage: `Player does not exist!` })
                return false;
        }
        for(var i =0; i < participants.length; i++){
            if(splitParticipants[i] != undefined)
                if(!PlayerService.existPlayer(this.state.players, splitParticipants[i])){
                    this.setState({errorMessage: `Player ${splitParticipants[i]} does not exist!` })
                    return false;
                }
        }
        return true;
    }

    validate = () => {
        if(this.state.participants == ''){
            this.setState({errorMessage:'Add participants'});
            return false;
        }
        if((this.state.participants.split(',').length < 3)){
            this.setState({errorMessage:'Add almos 3 participants'});
            return false;
        }
        if(!this.validateParticipants(this.state.participants)){
            return false;
        }
        this.setState({errorMessage:''});
        return true;
    }

    public render() {
        return (
            <div>
                <div className="gr-championshipform">
                    <h1>Championship</h1>
                    <h3>{`${this.props.match.params.name}  -  ${this.props.match.params.type}`}</h3>
                    
                    <textarea placeholder={"Player1,Player2,Player3,.."} name="participants" onChange={this.handleChange} value={this.state.participants}/>
                    <div>
                        {
                            <button className="gr-addChampionship" onClick={this.addChampionship}>Add</button>
                        }

                        {
                            this.state.errorMessage ?
                            <div className="gr-matchErrorMessage">
                                {this.state.errorMessage}
                            </div>:
                            ''
                        }
                    </div>
                </div>
                
            </div>
        )
    }
}