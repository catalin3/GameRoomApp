import * as React from 'react';
import './_championshipMenu.scss';
import Sidebar from '../Sidebar/Sidebar';
import ChampionshipContainer from '../ChampionshipContainer/ChampionshipContainer';

export interface IChampionshipMenuProps{
    //onChampionship
}

export interface IChampionshipMenuState{
    id: string;
    name: string;
    type: string;
}

export default class ChampionshipsMenu extends React.Component<any,IChampionshipMenuState>{
    constructor(props: any){
        super(props);
        this.state = {
            id: '',
            name: '',
            type: ''
        }
    }

    public gameSelected = (id: string, name: string, type: string) => {
        this.setState({
            id: id,
            name: name,
            type: type
        });
    }

    public render(){
        return (
            <div>
                <div className="gr-championship">
                    <h1>Championships</h1>
                    <Sidebar
                        onGameSelected = {this.gameSelected}
                    />
                    {
                        <ChampionshipContainer
                            idGame={this.state.id}
                            name={this.state.name}
                            type={this.state.type}
                        />
                    }
                </div>
               

            </div>
            
        )
    }
}