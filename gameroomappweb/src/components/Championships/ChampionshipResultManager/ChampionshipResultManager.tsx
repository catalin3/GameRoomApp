import * as React from 'react';
import './_championshipResultManager.scss';
//import './_championshipManager.scss';
import { RouteComponentProps } from 'react-router';
import { IChampionship } from 'src/models/IChampionship';
import { IMatch } from 'src/models/IMatch';
import ChampionshipService from 'src/services/ChampionshipService';
import MatchService from 'src/services/MatchService';
import ChampionshipResult from '../ChampionshipResult/ChampionshipResult';

export interface IChampionshipResultManagerState {
    championship: IChampionship;
    matches: IMatch[];
}

export interface IChampionshipResultManagerProps extends RouteComponentProps<any> {

}

export default class ChampionshipResultManager extends React.Component<IChampionshipResultManagerProps, any> {
    constructor(props: IChampionshipResultManagerProps){
        super(props);

        this.state = {
            championship: {
                id: '',
                name: '',
                type: '',
                participants: [],
                curentStatus: 0,
                firstPlace: '',
                secondPlace: '',
                thisrdPlace: ''
            },
            matches: []
        }
    }

    componentDidMount(){
        ChampionshipService.getChampionshipById(this.props.match.params.idChampionship)
            .then((result: IChampionship) => {
                this.setState({
                    championship: result
                })
            })
        MatchService.getMatchesByChampionship(this.props.match.params.idChampionship)
            .then((result: Array<IMatch>) => {
                this.setState({
                    matches: result
                });
            });
        
    }

    public render () {
        return (
            <div className="gr-championshipResultmanager">
                <ChampionshipResult
                    championship={this.state.championship}
                    matches={this.state.matches}
                />
            </div>
        )
    }

}