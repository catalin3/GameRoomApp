import * as React from 'react';
import './_championshipResult.scss';
import { IChampionship } from 'src/models/IChampionship';
import { IMatch } from 'src/models/IMatch';
import Match from 'src/components/GameContainer/Match/Match';
import { IStandingEntity } from 'src/models/IStandingEntity';
import PlayerService from 'src/services/PlayerService';
import ChampionshipService from 'src/services/ChampionshipService';

export interface IChampionshipResultProps {
    championship: IChampionship;
    matches: IMatch[];
}

export interface IChampionshipResultState {
    standingEntities: IStandingEntity[];
}

export default class ChampionshipResult extends React.Component<IChampionshipResultProps,IChampionshipResultState> {
    constructor(props: IChampionshipResultProps){
        super(props);

        this.state = {
            standingEntities: []
        }
    }

    componentWillUpdate(){
        PlayerService.getChampionshipStandings(this.props.championship.id)
            .then((result: IStandingEntity[]) => {
                this.setState({
                    standingEntities: result
                })
            })
    }

    public deleteMatch = (match1: string) => {}

    public render(){
        return (
            <div className="gr-championshipresult">
                <h1>{`${this.props.championship.name}  -  ${this.props.championship.type}`}</h1>
                <h2>{ChampionshipService.getStatus(this.props.championship.curentStatus)}</h2>

                <div className="gr-championship-standing">
                    <table className="gr-table-championship">
                        <thead className="gr-thead-championship">
                            <tr>
                                <th><strong>Nickname</strong></th>
                                <th>MP</th>
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>F</th>
                                <th>A</th>
                                <th><strong>Points</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                    
                            {this.state.standingEntities.map((item, index) => (
                                <tr key={item.nickname}>
                                <td><strong>{`${item.nickname}`}</strong></td>
                                <td>{`${item.matchesPlayed}`}</td>
                                <td>{`${item.wins}`}</td>
                                <td>{`${item.draws}`}</td>
                                <td>{`${item.losses}`}</td>
                                <td>{`${item.goalsFor}`}</td>
                                <td>{`${item.goalsAgainst}`}</td>
                                <td><strong>{`${item.points}`}</strong></td>
                            </tr>
                            ))}
                        </tbody> 
                    </table> 
                </div>
                <br/><br/>
                <div className="gr-championship-matchList">
                    {this.props.matches.map((item, index) => (
                        <div key={index}>
                            <Match
                                item={item}
                                deleteMatch={this.deleteMatch}
                            >
                            </Match>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}
