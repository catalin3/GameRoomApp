import * as React from 'react';
import './_matches.scss';
import Sidebar from '../Sidebar/Sidebar';
import GameContainer from '../GameContainer/GameContainer';
import { Redirect } from 'react-router';

export interface IHomeState {
    id: string;
    name: string;
    type: string;
    idMatch?: string;
}

export default class Matches extends React.Component<any, IHomeState>{

    constructor(props:any){
        super(props);
        this.state = {
            id: '',
            name: '',
            type: ''
        }
    }

    public gameSelected = (id: string, name: string, type: string) => {
        this.setState({
            id: id,
            name: name,
            type: type
        });
    }

    renderRedirect = () => {
        return <Redirect to='/login'/>
  }

    public render(){
        return( 
            <div>
                
                <div className="gr-matches">
                    <h1>Matches</h1>
                    <Sidebar onGameSelected={this.gameSelected} />
                    {
                        <GameContainer idGame={this.state.id} name={this.state.name} type={this.state.type} />
                    }
                </div>
                

            </div>
            
        )
    }
}