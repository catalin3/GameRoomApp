import * as React from 'react';
import { Link, Redirect } from 'react-router-dom';
import './_navigation.scss';
import UserService from 'src/services/UserService';

export interface INavigationProps{
    handleSetToken: (token: string)=> void;
}

export default class Navigation extends React.Component<INavigationProps,any> {
    
    constructor (props:INavigationProps){
        super(props);
    }

    handleLogout(e:any){
        UserService.logout();
        this.props.handleSetToken('');
    }

    renderRedirect = () => {
        return <Redirect to='/home'/>
    }

    public render(){
        return( 
            <nav className="main_nav">
                <button onClick={(e)=>{this.handleLogout(e)}} className="gr-logoutbtn">Logout</button>
                <div className="div link">
                <Link to='/home' className="link is-active">HOME</Link>
                </div>
                <Link to='/matches' className="link">Matches</Link>
                
                <Link to='/championships' className="link">Championships</Link>
            
                <Link to='/standings' className="link">Standings</Link>
            
                <Link to='/rules' className="link">Rules</Link>
            
                <Link to='/moments' className="link">Moments</Link>
            </nav>
        )
    }
}
