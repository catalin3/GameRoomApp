import * as React from 'react';
import Navigation from './Navigation/Navigation';
import './_header.scss'
import { Link, Redirect } from 'react-router-dom';


export interface IHeaderProps{
    handleSetToken: (token: string)=> void;
}

export interface IHeaderState{
    redirect: boolean;
    searchString: string;
}

export default class Header extends React.Component<IHeaderProps,IHeaderState>{
    constructor(props: IHeaderProps){
        super(props);
        this.state = {
            redirect: false,
            searchString: ''
        }
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IHeaderState) => (
        {
                ...prevState,
                [name]: value
        }));
    }

    redirectToSearch = () => {
        this.setState({
            redirect: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to={{
                    pathname:'/search',
                    state: {game: this.state.searchString}
        }} />
        }
        else
        return ""
    }


    public render() {
        return (
            <header className="main-header">
                <Navigation handleSetToken={this.props.handleSetToken}/>
                <div className="gr-logo">
                    <div>
                        <Link to="/home">
                            <img src={require('./Navigation/logo.png')} alt="game room logo"/>
                        </Link>
                    </div>
                </div>
                <div className="gr-search">
                    
                    <div>
                        {this.renderRedirect()}
                        <input type="text" name="searchString" placeholder="search" onChange={this.handleChange} value={this.state.searchString}/>
                        <button onClick={this.redirectToSearch}></button>
                    </div>
                </div>
            </header>
        );
    }
}

