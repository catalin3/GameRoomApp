import * as React from 'react';
import UserService from 'src/services/UserService';
import './_login.scss';

export interface ILoginProps{
    handleSetToken: (token: string)=> void;
    handleShowregisterComponent: (e: boolean)=>void;
}

export interface ILoginState{
    nickname: string;
    password: string;
    user: any;
    errorMessage: string;
}

export default class Login extends React.Component<ILoginProps,ILoginState>{
    constructor(props:ILoginProps){
        super(props);
        this.state = {
            nickname:'',
            password:'',
            user:null,
            errorMessage:''
        }
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: ILoginState) => (
        {
                ...prevState,
                [name]: value
            
        }));
    }

    login(nickname: string, password: string){
        if(this.validate()){
            UserService.login(nickname, password)
                .then(result => {
                    UserService.setToken(result.token)
                    this.setState({
                        user: result
                    })
                    this.props.handleSetToken(result.token);
                })
        }
    }
    handleRegister(e:any){
        this.props.handleShowregisterComponent(true);
    }

    validate = () => {
            if(this.state.nickname==''){
            this.setState({ errorMessage:'Add a nickname!' });
            return false;
        }
        if(this.state.password==''){
            this.setState({ errorMessage:'Add a Password!' });
            return false;
        }
        return true;
    }

    public render(){
        return (
                <div className="gr-logindiv">
                    <h1>Login</h1>
                    <div>
                        <input type="text" name="nickname" onChange={this.handleChange} value={this.state.nickname} placeholder={"Nickname"}/>
                    </div>
                    <div>
                        <input type="password" name="password" onChange={this.handleChange} value={this.state.password} placeholder={"********"}/>
                    </div>
                    <button 
                        className="loginBtn" 
                        onClick={() => this.login(this.state.nickname, this.state.password)}
                    >Login
                    </button>
                    {
                        this.state.errorMessage ?
                        <div className="gr-matchErrorMessage">
                            {this.state.errorMessage}
                        </div>:
                        ''
                    }
                    
                    <p>You don't have an account? Please <button onClick={(e)=>{this.handleRegister(e)}}>Register</button></p>
                </div>
        )
    }
}
