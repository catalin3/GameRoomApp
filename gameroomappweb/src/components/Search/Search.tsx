import * as React from 'react';
import './_search.scss';
import { IMatch } from 'src/models/IMatch';
import { IRule } from 'src/models/IRule';
import RuleService from 'src/services/RuleServices';
import GameService from 'src/services/GameService';
import { IGame } from 'src/models/IGame';
import Game from '../Sidebar/Game/game';
import Rule from '../Rules/Rule/Rule';

export interface ISearchState {
    rules: IRule[];
    games: IGame[];
    matches: IMatch[];
    loading: boolean;
}

export default class Search extends React.Component<any,ISearchState>{
    constructor(props: any){
        super(props);

        this.state = {
            matches: [],
            games: [],
            rules: [],
            loading: false
        }
    }

    componentDidMount() {
        this.setState({
            loading: true
        });
        RuleService.getRuleByGame(this.props.location.state.game)
            .then((result: IRule[]) => {
                this.setState({
                    rules: result,
                    loading: false
                });
            });
        GameService.getGameByName(this.props.location.state.game)
        .then((result: IGame[]) => {
            this.setState({
                games: result,
                loading: false
            });
        });
        
        
    }

    selectGame(){}
    deleteRule(){}

    public render() {
        return(
            <div className="gr-searchmenu">
                
                <h1 className="gr-searchtitle">{this.props.location.state.game}</h1>

                <h2 className="gr-searchtitle2">Games</h2>
                {this.state.loading && <h1>Loading</h1>}
                
                {!this.state.loading &&
                    this.state.games.map((item, index) => (
                        <div className="game" key={index}>
                            <Game
                                item={item}
                                selectGame={this.selectGame}
                            ></Game>
                        </div>
                    )
                    )}
                     <h2 className="gr-searchtitle2">Rules</h2>
                    {!this.state.loading &&
                    this.state.rules.map((item, index) => (
                            <Rule
                                item={item}
                                key={index}
                                deleteRule={this.deleteRule}
                            ></Rule>
                    )
                )}

            </div>
        )
    }
}