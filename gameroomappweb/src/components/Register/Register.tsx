import * as React from 'react';
import './_register.scss';
import PlayerService from 'src/services/PlayerService';

export interface IRegisterProps{
    handleRegister: (register:boolean)=> void;
}

export interface IRegisterState{
    nickname:string;
    firstName:string;
    lastName:string;
    password: string;
    passwordR: string;
    email:string;

    errorMessage:string;
}

export default class Register extends React.Component<IRegisterProps,IRegisterState>{
    constructor(props: IRegisterProps){
        super(props);
        this.state = {
            nickname:'',
            firstName:'',
            lastName:'',
            password:'',
            passwordR:'',
            email:'',
            errorMessage:''
        }
    }


    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IRegisterState) => (
        {
                ...prevState,
                [name]: value
            
        }));
    }

    register = (e:any) => {
        if(this.validate()){
            var newUser: IPlayer = {
                nickname: this.state.nickname,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                status: 'roky',
                experience: 0
            }
            PlayerService.addPlayer(newUser).then((result: IPlayer) => {
                console.log(result);
                this.props.handleRegister(false);
                alert("User Registered!");
            });
        }
    }

    validate = () => {
        if(this.state.nickname==''){
            this.setState({ errorMessage:'Add a nickname!' });
            return false;
        }
        if(this.state.firstName==''){
            this.setState({ errorMessage:'Add a First Name!' });
            return false;
        }
        if(this.state.lastName==''){
            this.setState({ errorMessage:'Add a Last Name!' });
            return false;
        }
        if(this.state.password==''){
            this.setState({ errorMessage:'Add a Password!' });
            return false;
        }
        if(this.state.passwordR==''){
            this.setState({ errorMessage:'Confirm Password!' });
            return false;
        }
        if(this.state.email==''){
            this.setState({ errorMessage:'Add Email!' });
            return false;
        }
        if(this.state.password != this.state.passwordR){
            this.setState({ errorMessage:`The Password doesn't match the conform Password!` });
            return false;
        }
        return true;
    }


    public render() {
        return(
            <div className="gr-registermenu">
                <h1>Register</h1>
                <input type="text" name="nickname" onChange={this.handleChange} value={this.state.nickname} placeholder={"Nickname"}/><br/>
                <input type="text" name="firstName" onChange={this.handleChange} value={this.state.firstName} placeholder={"First Name"}/><br/>
                <input type="text" name="lastName" onChange={this.handleChange} value={this.state.lastName} placeholder={"Last Name"}/><br/>
                <input type="password" name="password" onChange={this.handleChange} value={this.state.password} placeholder={"Password"}/><br/>
                <input type="password" name="passwordR" onChange={this.handleChange} value={this.state.passwordR} placeholder={"Retype Password"}/><br/>
                <input type="text" name="email" onChange={this.handleChange} value={this.state.email} placeholder={"Email"}/><br/>

                <button 
                    className="gr-registerbtn" 
                    onClick={(e) => this.register(e)}
                >Register</button>
                {
                        this.state.errorMessage ?
                        <div className="gr-matchErrorMessage">
                            {this.state.errorMessage}
                        </div>:
                        ''
                    }
            </div>
        )
    }
}
