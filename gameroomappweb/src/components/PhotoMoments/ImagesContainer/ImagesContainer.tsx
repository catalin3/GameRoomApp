import * as React from 'react';
import './_imageContainer.scss';
import { IImageMoment } from 'src/models/IImageModel';
import ImageService from 'src/services/ImageService';
import ImageMoment from '../ImageMoment/ImageMoment';

export interface IImagesContainerProps {
    nameOfGame: string;
}

export interface IImagesContainerState {
    images: Array<IImageMoment>;
    loading: boolean;
}

export default class ImagesContainer extends React.Component<IImagesContainerProps,IImagesContainerState> {
    constructor(props: IImagesContainerProps){
        super(props)

        this.state = {
            images: [],
            loading: false
        }
    }

    componentWillReceiveProps(props: IImagesContainerProps) {
        this.setState({
            loading: true
        });
        ImageService.getImagesByGame(props.nameOfGame)
            .then((result: Array<IImageMoment>) => {
                this.setState({
                    images: result,
                    loading: false
                });
            });
    }

    public deleteImage = (id: string) => {
        ImageService.deleteImage(id)
            .then(() => {
                this.setState((previousState: IImagesContainerState) => ({
                    images: [...previousState.images.filter(m => m.id != id)]
                }));
            })
    }

    public render() {
        return (
            this.props.nameOfGame !== '' &&
            <div className="gr-imagecontainer">
                {this.state.loading && <h3>Loading</h3>}
                
                {!this.state.loading &&
                    this.state.images.map((item,index) => (
                        <div key={index}>
                            <ImageMoment
                                item={item}
                                deleteImage={this.deleteImage}
                            >
                            </ImageMoment>
                        </div>
                    ))}

            </div>
        );
    }
}