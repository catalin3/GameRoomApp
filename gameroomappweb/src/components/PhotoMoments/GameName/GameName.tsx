import * as React from 'react';
import './_gameName.scss';

export interface IGameNameProps {
    item:string;
    selectGame(item: string): void;
}

const GameName  = (props: IGameNameProps) => {
    return(
        <div key={props.item} className="gr-main-game">
            <span className="gameName" onClick={() => {props.selectGame(props.item)}}>{props.item}</span>
        </div>
    );
}

export default GameName;