import * as React from 'react';
import './_imageSidebar.scss';
import ImageService from 'src/services/ImageService';
import GameName from '../GameName/GameName';
import { Link } from 'react-router-dom';

export interface IImageSidebarProps {
    onGameSelected(game: string): void;
}

export interface IImageSidebarState {
    games: string[];
    loading: boolean;
}

export default class ImageSidebar extends React.Component<IImageSidebarProps, IImageSidebarState>{
    constructor(props: any){
        super(props);

        this.state = {
            games: [],
            loading: true
        }
    }

    componentDidMount(){
        ImageService.getGamesName().then((games: string[]) => {
            this.setState({
                games: games,
                loading: false
            });
        });
    }

    public selectGame = (game: string) => {
        if(!game) return;
        this.props.onGameSelected(game);
    }

    public render() {
        return (
            <div className="gr-main-imageSidebar">
                <div >
                    <Link className="gr-button-addImage" to="/addImage">AddImage</Link>
                </div>
                <br/>
                <br/>
                <div className="gr-imageSidebar">
                    <h1>Games</h1>
                    {this.state.loading && <h1>Loading</h1>}
                    {!this.state.loading && 
                        this.state.games.map((item,index) => (
                                <GameName 
                                    key={index}
                                    item={item}
                                    selectGame={this.selectGame}
                                ></GameName>
                        )
                    )}
                </div>
            </div>

        );
    }
}