import * as React from 'react';
import './_imageManager.scss';
import { IImageMoment } from 'src/models/IImageModel';
import * as mongoose from 'mongoose';
import ImageService from 'src/services/ImageService';
import { RouteComponentProps } from 'react-router';
import PredictionService from 'src/services/PredictionService';

export interface IImageManagerState{
    description: string;
    nameOfGame: string;
    image: string;
    errorMessage: string;
    games: string[];
}

export interface IImageManagerProps extends RouteComponentProps<any>{
}

export default class ImageManager extends React.Component<IImageManagerProps,IImageManagerState> {
    constructor(props: IImageManagerProps){
        super(props);

        this.state = {
            description:'',
            nameOfGame:'',
            image:'',
            errorMessage:'',
            games: []
        }
    }

    componentDidMount(){
        ImageService.getGamesName()
            .then((games: string[]) => {
                this.setState({
                    games: games
                })
            })
    }

    handleImage = (e:any) => {
        var reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image: imageDataAsString
            })
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IImageManagerState) => (
        {
                ...prevState,
                [name]: value
            
        }));
    }

    public addImage = () => {
        if(this.validate()){
            var newImage: IImageMoment = {
                id: String(mongoose.Types.ObjectId()),
                name: "image",
                game: this.state.nameOfGame,
                imageDataAsString: this.state.image,
                description: this.state.description
            }
            if(this.state.nameOfGame == ""){
                //var categoryResult: any = this.getImageCategory(this.state.image);
                fetch(this.state.image)
                .then(res => res.blob())
                .then(blob => 
                    PredictionService.getCategory(blob)
                        .then((rez: any) => {
                            const category = rez.predictions.reduce(function(prev: any, current:any) {
                                return (prev.probability > current.probability) ? prev : current
                            }) 
                            console.log(category);
                            alert("The Image was added to "+category.tagName);
                            newImage.game = category.tagName
                            console.log(newImage);
                            ImageService.addImage(newImage)
                            .then(() => {
                                this.props.history.goBack();
                            });
                        })
                    )
            }
            else{
                ImageService.addImage(newImage)
                            .then(() => {
                                this.props.history.push('/moments')
                            });
            }
            
      
        }
    }

    validate = () => {
        if(this.state.image == ''){
            this.setState({errorMessage:`Add an image!!`});
            return false;
        }
        this.setState({errorMessage:''});
        return true;
    }

    public render() {
        return (
            <div>
                <div className="gr-imageform">
                    <h1>Moment</h1>
                    <textarea  
                        name="description" 
                        onChange={this.handleChange}
                        value={this.state.description}
                        placeholder={"Add a description"}
                    />
                    <br/>
                    <input 
                        type="text" 
                        name="nameOfGame" 
                        onChange={this.handleChange} 
                        value={this.state.nameOfGame} 
                        placeholder={"Game"}
                    /><p>Automatic insert for: Darts, Fussball, Table, Cards</p>
                    <br/>
                    <input 
                        id="imaginetare"
                        type="file" 
                        name="image" 
                        onChange={(e)=>this.handleImage(e)}  
                    />
                
                <div className="gr-imagemanager">
                {
                    <div>
                        <br/>
                        <button className="gr-addImage" onClick={this.addImage}>Add</button>
                    </div>
                }
                {
                    this.state.errorMessage ?
                    <div className="gr-matchErrorMessage">
                        {this.state.errorMessage}
                    </div>:
                    ''
                }
                </div>
                </div>
            </div>
        );
    }
}