import * as React from 'react';
import './_imageMoment.scss';
import { IImageMoment } from 'src/models/IImageModel';

export interface IImageMomentProps {
    item: IImageMoment;
    deleteImage(id: string): void;
}

const ImageMoment  = (props: IImageMomentProps) => {
    return(
        <div key={props.item.id} className="gr-imageMoment">
            <img src={props.item.imageDataAsString} />
            <p>{props.item.description}</p>
            <button onClick={() => props.deleteImage(props.item.id)}>Delete</button>
        </div>
    );
}

export default ImageMoment;