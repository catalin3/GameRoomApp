import * as React from 'react';
import './_imageMenu.scss';
import ImageSidebar from './ImagesSideBar/ImageSidebar';
import ImagesContainer from './ImagesContainer/ImagesContainer';


export interface IImageMenuState {
    nameOfGame: string;
}

export default class ImageMenu extends React.Component<any,IImageMenuState> {
    constructor(props: any){
        super(props);
        
        this.state = {
            nameOfGame: ''
        }
    }

    public gameSelected = (nameOfGame: string) => {
        this.setState({
            nameOfGame: nameOfGame
        });
    }

    public render() {
        return (
                <div className="gr-moments">
                    <h1>Moments</h1>
                        <ImageSidebar onGameSelected={this.gameSelected}/>
                        {
                            <ImagesContainer nameOfGame={this.state.nameOfGame}/>
                        }
                </div>
                
            
        )
    }


}