import * as React from 'react';
import './_rule.scss';
import { IRule } from 'src/models/IRule';

export interface IRuleProps {
    item: IRule;
    key:any;
    deleteRule(id: string): void;
}

const Rule  = (props: IRuleProps) => {
    return(
        <div key={props.item.id} className="gr-rule">
            <div className="gr-rulemain">
                <button onClick={() => props.deleteRule(props.item.id)}>Delete</button>
                <h2>{props.item.game} - {props.item.type}</h2>
            </div>    
            <p>{props.item.description}</p>
            
        </div>
    );
}

export default Rule;