import * as React from 'react';
import { IGame } from 'src/models/IGame';
import { IRule } from 'src/models/IRule';
import { RouteComponentProps } from 'react-router';
import RuleService from 'src/services/RuleServices';
import * as mongoose from 'mongoose';


export interface IRuleManagerState{
    games: IGame[];
    rules: IRule[];
    errorMessage: string;

    game: string;
    type: string;
    description: string;
    dateAdded: string;
}

export interface IRuleManagerProps extends RouteComponentProps{}

export default class RuleManager extends React.Component<IRuleManagerProps,IRuleManagerState>{
    constructor(props: IRuleManagerProps){
        super(props);
        this.state = {
            games: [],
            rules: [],
            errorMessage: '',
            game: '',
            type: '',
            description: '',
            dateAdded: ''
        }
    }

    componentDidMount(){
        //GameService.getAllGames()
        //    .then((games: IGame[]) => {
        //        this.setState({
        //            games: games
        //        })
        //    })
        RuleService.getAllRules()
            .then((rules: IRule[]) => {
                this.setState({
                    rules: rules
                })
            })
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IRuleManagerState) => (
        {
                ...prevState,
                [name]: value
            
        }));
    }

    public addRule = () => {
        if(this.validate()){
            var newRule: IRule = {
                id: String(mongoose.Types.ObjectId()),
                game: this.state.game,
                type: this.state.type,
                description: this.state.description
            }
            
            RuleService.insertRule(newRule)
                .then(() => {
                    this.props.history.push('/rules')
                });
        }
    }

    validate = () => {
        if(this.state.game == ''){
            this.setState({errorMessage:`Add a Game!!`});
            return false;
        }
        if(this.state.type == ''){
            this.setState({errorMessage:`Add a Type!!`});
            return false;
        }
        if(this.state.description == ''){
            this.setState({errorMessage:`Add a Description!!`});
            return false;
        }
        for(var i =0; i < this.state.rules.length; i++){
            if(this.state.game == this.state.rules[i].game && this.state.type == this.state.rules[i].type){
                this.setState({errorMessage: `Already exist a Rule for this Game!` })
                return false;
            }
        }
        this.setState({errorMessage:''});
        return true;
    }

    public render() {
        return (
            <div>
                <div className="gr-matchform">
                    <h1>Rule</h1>
                    <input 
                        type="text" 
                        name="game" 
                        onChange={this.handleChange} 
                        value={this.state.game} 
                        placeholder={"Name Of Game"}
                    />
                    <br/>
                    <input 
                        type="text" 
                        name="type" 
                        onChange={this.handleChange} 
                        value={this.state.type} 
                        placeholder={"Type Of Game"}
                    />
                    <br/>
                    <textarea  
                        name="description" 
                        onChange={this.handleChange}
                        value={this.state.description}
                        placeholder={"Add a description"}
                    />
                
                   
                <div className="gr-imagemanager">
                {
                    <div>
                        <br/>
                        <button className="gr-addImage" onClick={this.addRule}>Add</button>
                    </div>
                }
                {
                    this.state.errorMessage ?
                    <div className="gr-matchErrorMessage">
                        {this.state.errorMessage}
                    </div>:
                    ''
                }
                </div>
                </div>
            </div>
        );
    }
}