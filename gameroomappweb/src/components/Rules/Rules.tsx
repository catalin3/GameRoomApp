import * as React from 'react';
import './_rules.scss';
import { IRule } from 'src/models/IRule';
import RuleService from 'src/services/RuleServices';
import Rule from './Rule/Rule';
import { Link } from 'react-router-dom';

export interface IRulesProps{}

export interface IRulesState{
    rules: IRule[],
    loading: boolean;
}

class Rules extends React.Component<IRulesProps,IRulesState>{
    
    constructor(props:IRulesProps){
        super(props);

        this.state = {
            loading: true,
            rules: []
        }
    }

    componentDidMount(){
        this.setState({
            loading: false
        });
        RuleService.getAllRules().then((result: IRule[]) => {
            this.setState({
                rules: result
            });
        });
    }

    public deleteRule = (id: string) => {
        RuleService.deleteRule(id)
            .then(() => {
                this.setState((previousState: IRulesState) => ({
                    rules: [...previousState.rules.filter(m => m.id != id)]
                }));
            })
    }

    public render(){
        return(
                <div className="gr-rules-main">
                    <div className="gr-div-button">
                        <Link className="gr-button-addMatch" to='addRule'>Add Rule</Link>
                    </div>
                    {this.state.loading && <h3>Loading</h3>}
                    
                    {!this.state.loading &&
                        this.state.rules.map((item,index) => (
                            <Rule
                                key={index}
                                item={item}
                                deleteRule={this.deleteRule}
                            >
                            </Rule>
                        ))}

                </div>
            
        )
    }
}

export default Rules;