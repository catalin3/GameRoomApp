import * as React from 'react';
import './_carousel.scss';
import Slider from 'react-slick';


export default class CarouselSlider extends React.Component<any, any>{

    constructor(props:any){
        super(props);
        this.state = {
            
        }
    }

    public render(){
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay:true,
          };
          return (
            <div className="gr-carousel-slider">
              <Slider {...settings}>
                <div className="carousel-logo">
                  <img src={require('./logo.png')} alt="game room logo"/>
                </div>
                <div>
                  <img src={require('./darts.jpg')} alt="game room logo"/>
                </div>
                <div>
                  <img src={require('./catan.jpg')} alt="game room logo"/>
                </div>
                <div>
                  <img src={require('./fussball.jpg')} alt="game room logo"/>
                </div>
                <div>
                  <img src={require('./uno.jpg')} alt="game room logo"/>
                </div>
                
              </Slider>
            </div>
          );
    }
}