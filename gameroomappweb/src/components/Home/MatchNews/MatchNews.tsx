import * as React from 'react';
import './_matchNews.scss';
import { IMatch } from 'src/models/IMatch';
import MatchService from 'src/services/MatchService';

export interface IMatchNewsState {
    matches: IMatch[];
    loading: boolean;
}

export default class MatchNews extends React.Component<any, IMatchNewsState>{

    constructor(props:any){
        super(props);
        this.state = {
            matches: [],
            loading:false
        }
    }

    componentDidMount(){
        MatchService.getLast10().then((matches: IMatch[]) => {
            this.setState({
                matches: matches,
                loading: false
            });
        });
    }

    public render(){
        return(
            <div className="gr-matchnews">
                <h2>Last 10 matches</h2>
                {this.state.loading && <h1>Loading</h1>}
                {!this.state.loading &&
                    this.state.matches.map((item, index) => (
                            <div className="gr-match-details">
                                <strong>{`[${item.homeTeam}]`}</strong>    
                                <p className="score">{` ${item.scoreHomeTeam}  -  ${item.scoreAwayTeam} `}</p>     
                                <strong>{`[${item.awayTeam}]`}</strong>
                             </div>
                    )
                    )}
            </div>
        )
    }
}