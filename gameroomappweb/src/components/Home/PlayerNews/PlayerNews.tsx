import * as React from 'react';
import './_playerNews.scss';
import PlayerService from 'src/services/PlayerService';

export interface IPlayerNewsState {
    players: IPlayer[];
    loading: boolean;
}

export default class PlayerNews extends React.Component<any, any>{

    constructor(props:any){
        super(props);
        this.state = {
            players: [],
            loading:false
        }
    }

    componentDidMount(){
        PlayerService.getPlayersPage(0,10,"Experience","Descending").then((players: IPlayer[]) => {
            this.setState({
                players: players,
                loading: false
            });
        });
    }

    public render(){
        return(
            <div className="gr-playernews">
                <h2>Top 10</h2>
                {this.state.loading && <h1>Loading</h1>}
                <table className="gr-table-standings">
                    <thead className="">
                        <tr>
                            <th>Nickname</th>
                            <th>Status</th>
                            <th>Experience</th>
                        </tr>
                    </thead>
                    <tbody>
                    

                    {!this.state.loading &&
                        this.state.players.map((item: IPlayer, index: number) => (
                            <tr key={index}>
                                <td>{`${item.nickname}`}</td>
                                <td>{`${item.status}`}</td>
                                <td>{`${item.experience}`}</td>
                            </tr>
                        )
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}