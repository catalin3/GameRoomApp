import * as React from 'react';
import './_momentNews.scss';
import ImageService from 'src/services/ImageService';
import { IImageMoment } from 'src/models/IImageModel';

export interface IMomentNewsState {
    images: IImageMoment[];
    loading: boolean;
}

export default class MomentNews extends React.Component<any, IMomentNewsState>{

    constructor(props:any){
        super(props);
        this.state = {
            images:[],
            loading:false
        }
    }

    componentDidMount(){
        this.setState({
            loading: true
        });
        ImageService.getLast10().then((images: IImageMoment[]) => {
            this.setState({
                images:images,
                loading:false
            })
        })
    }

    public render(){
        return(
            <div className="gr-momentnews">
                <h2>Last Moments</h2>
                {this.state.loading && <h3>Loading</h3>}
                
                {!this.state.loading &&
                    this.state.images.map((item,index) => (
                        <div key={item.id} className="gr-imageMoment">
                            <img src={item.imageDataAsString} />
                            <p>{item.description}</p>
                               
                        </div>
                    ))}
            </div>
        )
    }
}