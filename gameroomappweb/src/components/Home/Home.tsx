import * as React from 'react';
import './_home.scss';
import CarouselSlider from './Carousel/CarouselSlider';
import MatchNews from './MatchNews/MatchNews';
import MomentNews from './MomentNews/MomentNews';
import PlayerNews from './PlayerNews/PlayerNews';

export default class Home extends React.Component<any, any>{

    constructor(props:any){
        super(props);
        this.state = {
            
        }
    }

    public render(){
        return(
            <div className="gr-home">
                <h1>Home</h1>
                <CarouselSlider />
                <div className="gr-news">
                    <PlayerNews/>
                    <MatchNews/> 
                </div>
                <MomentNews/>
            </div>
        )
    }
}