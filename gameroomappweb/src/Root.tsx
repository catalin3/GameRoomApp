import * as React from 'react';
import './_root.scss';
import { Switch, Route } from 'react-router';
import Home from './components/Home/Home';
import Rules from './components/Rules/Rules';
import GameSidebarManager from './components/SidebarManager/GameSidebarManager';
import Standings from './components/Standings/Standings';
import Profile from './components/User/Profile/Profile';
import MatchManager from './components/GameContainer/Match/MatchManager/MatchManager';
import ChampionshipsMenu from './components/Championships/ChampionshipMenu';
import ChampionshipManager from './components/Championships/ChampionshipsManager/ChampionshipManager';
import ChampionshipResultManager from './components/Championships/ChampionshipResultManager/ChampionshipResultManager';
import ImageMenu from './components/PhotoMoments/ImageMenu';
import ImageManager from './components/PhotoMoments/ImageManager/ImageManager';
import Matches from './components/Matches/Matches';
import Search from './components/Search/Search';
import RuleManager from './components/Rules/RuleManager/RuleManager';
import Login from './components/Login/Login';
import Register from './components/Register/Register';


const Root = () => (
    <main className="gr-main">
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/home" component={Home}/>
            <Route path="/matches" component={Matches}/>
            <Route path="/rules" component={Rules}/>
            <Route path="/game" component={GameSidebarManager}/>
            <Route path="/championships" component={ChampionshipsMenu}/>
            <Route path="/standings" component={Standings}/>
            <Route path="/profile" component={Profile}/>
            <Route path="/match/:id/:idGame/:idChampionship" component={MatchManager}/>
            <Route path="/championship/:name/:type" component={ChampionshipManager}/>
            <Route path="/championshipresult/:idChampionship" component={ChampionshipResultManager}/>
            <Route path="/moments" component={ImageMenu}/>
            <Route path="/addImage" component={ImageManager}/>
            <Route path="/addRule" component={RuleManager}/>
            <Route path="/search" component={Search}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
        </Switch>
    </main>
)

export default Root;