import * as React from 'react';
import './_app.scss';
import MainComponent from './components/MainComponent/MainComponent';
import Login from './components/Login/Login';
import Register from './components/Register/Register';

export interface IAppState{
  token:string;
  showRegisterComponent: boolean;
}

export default class App extends React.Component<any,IAppState>{
  constructor(props: any){
    super(props);
    this.state = {
      token:'',
      showRegisterComponent:false
    }
  }

  handleShowregisterComponent = (e: boolean) => {
    this.setState(
    {
      showRegisterComponent:e  
    });
  } 

  handleSetToken = (token: string) => {
    this.setState({
      token: token
    })
  }

  handleRegister = (register: boolean) => {
    this.setState({
      showRegisterComponent:false
    })
  }

  

  public render(){
    return (
        this.state.token != '' || localStorage.getItem("token") != null?
        <MainComponent handleSetToken={this.handleSetToken}/>:
        (
          this.state.showRegisterComponent==true?
            <Register handleRegister={this.handleRegister}/>:
            <Login handleSetToken={this.handleSetToken} handleShowregisterComponent={this.handleShowregisterComponent}/>
        )
    );
  }

}
